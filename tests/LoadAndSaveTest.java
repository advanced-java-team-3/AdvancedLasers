import core.Game;
import data.LaserColor;
import javafx.geometry.Point3D;
import mockit.Injectable;
import objects.GameObject;
import objects.LaserPointer;
import org.junit.Assert;
import org.junit.Test;
import core.World;

/**
 * Created by Daniel on 10/7/2016.
 */
public class LoadAndSaveTest {

    @Injectable
    private Game game;

    @Test
    public void playersSaveNeverNull() {

        World world = new World(game, 10, 10);

        //check that the players save is not null (if none exists it creates a new one)
        Assert.assertNotEquals(null, world.getPlayersSave());

    }
}