import core.Game;
import graphics.Widget;
import javafx.geometry.Point3D;
import mockit.Injectable;
import objects.GameObject;
import org.junit.Assert;
import org.junit.Test;
import core.World;

/**
 * Created by shaun on 24/09/2016.
 */
public class WorldTest {
    @Test
    public void add(@Injectable Game game,
                    @Injectable Widget<GameObject> widget) {

        World world = new World(game, 10, 10);
        GameObject go = new GameObject(game, widget);

        go.setPosition(9, 9, 1);
        world.add(go);
        Assert.assertEquals(new Point3D(10, 10, 1), go.getPosition());
        world.remove(go);

        go.setPosition(15.1, 15.1, 19);
        world.add(go);
        Assert.assertEquals(new Point3D(20, 20, 19), go.getPosition());
        world.remove(go);

        go.setPosition(-95.01, -95.01, -5);
        world.add(go);
        Assert.assertEquals(new Point3D(-100, -100, -5), go.getPosition());
        world.remove(go);

        go.setPosition(0, 0, 0);
        world.add(go);
        Assert.assertEquals(new Point3D(0, 0, 0), go.getPosition());
    }
}
