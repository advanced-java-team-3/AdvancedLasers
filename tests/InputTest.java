import core.Input;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mock;
import mockit.MockUp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by shaun on 16/09/2016.
 */
public class InputTest {
    private EventHandler<? super KeyEvent> keyPressedHandler;
    private EventHandler<? super KeyEvent> keyReleasedHandler;
    private EventHandler<? super MouseEvent> mousePressedHandler;
    private EventHandler<? super MouseEvent> mouseReleasedHandler;
    private EventHandler<? super MouseEvent> mouseMovedHandler;
    private EventHandler<? super MouseEvent> mouseDraggedHandler;

    private Input input;

    @Before
    public void setUp() {
        input = new Input(this.new FakeNode().getMockInstance());
    }

    @Test
    public void initialization() {
        Assert.assertNotNull(keyPressedHandler);
        Assert.assertNotNull(keyReleasedHandler);
        Assert.assertNotNull(mousePressedHandler);
        Assert.assertNotNull(mouseReleasedHandler);
        Assert.assertNotNull(mouseMovedHandler);
        Assert.assertNotNull(mouseDraggedHandler);
    }

    @Test
    public void keyDown(@Injectable KeyEvent aKeyEvent) {
        new Expectations() {{
            aKeyEvent.getCode();
            result = KeyCode.A;
        }};

        // key is initially not down
        Assert.assertFalse(input.keyDown(KeyCode.A));

        // key press event occurs
        keyPressedHandler.handle(aKeyEvent);
        Assert.assertTrue(input.keyDown(KeyCode.A));

        // key release event occurs
        keyReleasedHandler.handle(aKeyEvent);
        Assert.assertFalse(input.keyDown(KeyCode.A));

        // key press event occurs
        keyPressedHandler.handle(aKeyEvent);
        Assert.assertTrue(input.keyDown(KeyCode.A));

        // update occurs
        input.afterUpdate();
        Assert.assertTrue(input.keyDown(KeyCode.A));

        // key release event occurs
        keyReleasedHandler.handle(aKeyEvent);
        Assert.assertFalse(input.keyDown(KeyCode.A));
    }

    @Test
    public void keyPressed(@Injectable KeyEvent aKeyEvent) {
        new Expectations() {{
            aKeyEvent.getCode();
            result = KeyCode.A;
        }};

        // key is initially not pressed
        Assert.assertFalse(input.keyPressed(KeyCode.A));

        // key press event occurs
        keyPressedHandler.handle(aKeyEvent);
        Assert.assertTrue(input.keyPressed(KeyCode.A));

        // update occurs, key was not pressed this tick
        input.afterUpdate();
        Assert.assertFalse(input.keyPressed(KeyCode.A));

        // key is released
        keyReleasedHandler.handle(aKeyEvent);
        Assert.assertFalse(input.keyPressed(KeyCode.A));

        // update occurs
        input.afterUpdate();

        // key press, followed by key release. Key should not be pressed
        keyPressedHandler.handle(aKeyEvent);
        keyReleasedHandler.handle(aKeyEvent);
        Assert.assertFalse(input.keyPressed(KeyCode.A));
    }

    @Test
    public void keyReleased(@Injectable KeyEvent aKeyEvent) {
        new Expectations() {{
            aKeyEvent.getCode();
            result = KeyCode.A;
        }};

        // key is initially not pressed
        Assert.assertFalse(input.keyReleased(KeyCode.A));

        // key press event occurs
        keyPressedHandler.handle(aKeyEvent);
        Assert.assertFalse(input.keyReleased(KeyCode.A));

        // update occurs, key was not pressed this tick
        input.afterUpdate();
        Assert.assertFalse(input.keyReleased(KeyCode.A));

        // key is released
        keyReleasedHandler.handle(aKeyEvent);
        Assert.assertTrue(input.keyReleased(KeyCode.A));

        // update occurs
        input.afterUpdate();
        Assert.assertFalse(input.keyReleased(KeyCode.A));

        // key press, followed by key release. Key should not be pressed
        keyPressedHandler.handle(aKeyEvent);
        keyReleasedHandler.handle(aKeyEvent);
        Assert.assertTrue(input.keyReleased(KeyCode.A));
    }

    @Test
    public void mouseDown(@Injectable MouseEvent mouseEvent) {
        new Expectations() {{
            mouseEvent.getButton();
            result = MouseButton.PRIMARY;
        }};

        // mouse is initially not down
        Assert.assertFalse(input.mouseDown());

        // mouse press event occurs
        mousePressedHandler.handle(mouseEvent);
        Assert.assertTrue(input.mouseDown());

        // mouse release event occurs
        mouseReleasedHandler.handle(mouseEvent);
        Assert.assertFalse(input.mouseDown());

        // mouse press event occurs
        mousePressedHandler.handle(mouseEvent);
        Assert.assertTrue(input.mouseDown());

        // update occurs
        input.afterUpdate();
        Assert.assertTrue(input.mouseDown());

        // mouse release event occurs
        mouseReleasedHandler.handle(mouseEvent);
        Assert.assertFalse(input.mouseDown());
    }

    @Test
    public void mousePressed(@Injectable MouseEvent mouseEvent) {
        new Expectations() {{
            mouseEvent.getButton();
            result = MouseButton.PRIMARY;
        }};

        // mouse is initially not down
        Assert.assertFalse(input.mousePressed());

        // mouse press event occurs
        mousePressedHandler.handle(mouseEvent);
        Assert.assertTrue(input.mousePressed());

        // mouse release event occurs
        mouseReleasedHandler.handle(mouseEvent);
        Assert.assertFalse(input.mousePressed());

        // mouse press event occurs
        mousePressedHandler.handle(mouseEvent);
        Assert.assertTrue(input.mousePressed());

        // update occurs
        input.afterUpdate();
        Assert.assertFalse(input.mousePressed());

        // mouse release event occurs
        mouseReleasedHandler.handle(mouseEvent);
        Assert.assertFalse(input.mousePressed());
    }

    @Test
    public void mouseReleased(@Injectable MouseEvent mouseEvent) {
        new Expectations() {{
            mouseEvent.getButton();
            result = MouseButton.PRIMARY;
        }};

        // mouse is initially not down
        Assert.assertFalse(input.mouseReleased());

        // mouse press event occurs
        mousePressedHandler.handle(mouseEvent);
        Assert.assertFalse(input.mouseReleased());

        // mouse release event occurs
        mouseReleasedHandler.handle(mouseEvent);
        Assert.assertTrue(input.mouseReleased());

        // mouse press event occurs
        mousePressedHandler.handle(mouseEvent);
        Assert.assertFalse(input.mouseReleased());

        // update occurs
        input.afterUpdate();
        Assert.assertFalse(input.mouseReleased());

        // mouse release event occurs
        mouseReleasedHandler.handle(mouseEvent);
        Assert.assertTrue(input.mouseReleased());

        // another update
        input.afterUpdate();
        Assert.assertFalse(input.mouseReleased());
    }

    @Test
    public void mousePosition(@Injectable MouseEvent mouseEvent) {
        new Expectations() {{
            mouseEvent.getX();
            result = 10.0;
            mouseEvent.getY();
            result = 10.0;
        }};

        // initially, the mouse position is (0, 0)
        Assert.assertEquals(new Point2D(0, 0), input.mousePosition());

        // mouse movement occurs
        mouseMovedHandler.handle(mouseEvent);
        Assert.assertEquals(new Point2D(10, 10), input.mousePosition());

        // mouse drag occurs
        new Expectations() {{
            mouseEvent.getX();
            result = 5.0;
            mouseEvent.getY();
            result = 5.0;
        }};

        mouseDraggedHandler.handle(mouseEvent);
        Assert.assertEquals(new Point2D(5, 5), input.mousePosition());
    }

    private class FakeScene extends MockUp<Scene> {
        @Mock
        public void setOnKeyPressed(EventHandler<? super KeyEvent> e) {
            keyPressedHandler = e;
        }
        @Mock
        public void setOnKeyReleased(EventHandler<? super KeyEvent> e) {
            keyReleasedHandler = e;
        }
    }

    private class FakeNode extends MockUp<Node> {

        private FakeScene scene = new FakeScene();

        @Mock
        public Scene getScene() {
            return scene.getMockInstance();
        }
        @Mock
        public void setOnMousePressed(EventHandler<? super MouseEvent> e) {
            mousePressedHandler = e;
        }
        @Mock
        public void setOnMouseReleased(EventHandler<? super MouseEvent> e) {
            mouseReleasedHandler = e;
        }
        @Mock
        public void setOnMouseMoved(EventHandler<? super MouseEvent> e) {
            mouseMovedHandler = e;
        }
        @Mock
        public void setOnMouseDragged(EventHandler<? super MouseEvent> e) {
            mouseDraggedHandler = e;
        }
    }
}