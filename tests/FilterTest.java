import core.Game;
import data.LaserColor;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Verifications;
import objects.Filter;
import objects.GameObject;
import objects.Laser;
import org.junit.Test;

import java.awt.geom.Line2D;

/**
 * Created by shaun on 6/10/2016.
 */
public class FilterTest {

    @Injectable
    private Game game;

    @Injectable
    private Laser laser;

    @Test
    public void sameColorPassesThrough() {
        assertCreatesLaser(
                LaserColor.RED, LaserColor.RED, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.GREEN, LaserColor.GREEN, LaserColor.GREEN);
        assertCreatesLaser(
                LaserColor.BLUE, LaserColor.BLUE, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.CYAN, LaserColor.CYAN, LaserColor.CYAN);
        assertCreatesLaser(
                LaserColor.MAGENTA, LaserColor.MAGENTA, LaserColor.MAGENTA);
        assertCreatesLaser(
                LaserColor.YELLOW, LaserColor.YELLOW, LaserColor.YELLOW);
    }

    @Test
    public void whitePassesThrough() {
        assertCreatesLaser(
                LaserColor.RED, LaserColor.WHITE, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.GREEN, LaserColor.WHITE, LaserColor.GREEN);
        assertCreatesLaser(
                LaserColor.BLUE, LaserColor.WHITE, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.CYAN, LaserColor.WHITE, LaserColor.CYAN);
        assertCreatesLaser(
                LaserColor.MAGENTA, LaserColor.WHITE, LaserColor.MAGENTA);
        assertCreatesLaser(
                LaserColor.YELLOW, LaserColor.WHITE, LaserColor.YELLOW);
    }

    @Test
    public void secondaryPassesThroughMatchingPrimary() {
        assertCreatesLaser(
                LaserColor.GREEN, LaserColor.CYAN, LaserColor.GREEN);
        assertCreatesLaser(
                LaserColor.BLUE, LaserColor.CYAN, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.BLUE, LaserColor.MAGENTA, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.RED, LaserColor.MAGENTA, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.RED, LaserColor.YELLOW, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.GREEN, LaserColor.YELLOW, LaserColor.GREEN);
    }

    @Test
    public void secondaryPassesThroughSecondary() {
        assertCreatesLaser(
                LaserColor.MAGENTA, LaserColor.CYAN, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.CYAN, LaserColor.MAGENTA, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.MAGENTA, LaserColor.YELLOW, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.YELLOW, LaserColor.MAGENTA, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.CYAN, LaserColor.YELLOW, LaserColor.GREEN);
        assertCreatesLaser(
                LaserColor.YELLOW, LaserColor.CYAN, LaserColor.GREEN);
    }

    @Test
    public void primaryPassesThroughMatchingSecondary() {
        assertCreatesLaser(
                LaserColor.YELLOW, LaserColor.RED, LaserColor.RED);
        assertCreatesLaser(
                LaserColor.YELLOW, LaserColor.GREEN, LaserColor.GREEN);
        assertCreatesLaser(
                LaserColor.CYAN, LaserColor.GREEN, LaserColor.GREEN);
        assertCreatesLaser(
                LaserColor.CYAN, LaserColor.BLUE, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.MAGENTA, LaserColor.BLUE, LaserColor.BLUE);
        assertCreatesLaser(
                LaserColor.MAGENTA, LaserColor.RED, LaserColor.RED);
    }

    @Test
    public void primaryBlocksPrimary() {
        assertCreatesLaser(LaserColor.GREEN, LaserColor.RED, null);
        assertCreatesLaser(LaserColor.RED, LaserColor.GREEN, null);
        assertCreatesLaser(LaserColor.GREEN, LaserColor.BLUE, null);
        assertCreatesLaser(LaserColor.BLUE, LaserColor.GREEN, null);
        assertCreatesLaser(LaserColor.BLUE, LaserColor.RED, null);
        assertCreatesLaser(LaserColor.RED, LaserColor.BLUE, null);
    }

    @Test
    public void primaryBlocksNonMatchingSecondary() {
        assertCreatesLaser(LaserColor.RED, LaserColor.CYAN, null);
        assertCreatesLaser(LaserColor.GREEN, LaserColor.MAGENTA, null);
        assertCreatesLaser(LaserColor.BLUE, LaserColor.YELLOW, null);
    }

    @Test
    public void secondaryBlocksNonMatchingPrimary() {
        assertCreatesLaser(LaserColor.CYAN, LaserColor.RED, null);
        assertCreatesLaser(LaserColor.MAGENTA, LaserColor.GREEN, null);
        assertCreatesLaser(LaserColor.BLUE, LaserColor.YELLOW, null);
    }

    /**
     * Assert that the expected laser is created or no laser if
     * <code>null</code> is specified.
     * @param filterColor The color of the filter.
     * @param laserColor The color of the incoming laser.
     * @param expected The expected color of the resulting laser, or
     *                 <code>null</code> for no laser.
     */
    private void assertCreatesLaser(LaserColor filterColor,
                                    LaserColor laserColor,
                                    LaserColor expected) {

        if (expected != null) {
            new Expectations() {{
                laser.createChild(
                        (GameObject) any, anyDouble, (LaserColor) any);

                laser.getColor();
                result = laserColor;
            }};
        }

        Filter filter = new Filter(game, filterColor);
        filter.onLaserHit(laser, new Line2D.Double(0, 0, 0, 0));

        if (expected == null) {
            new Verifications() {{
                laser.createChild(
                        (GameObject) any, anyDouble, (LaserColor) any);
                times = 0;
            }};
        }
        else {
            new Verifications() {{
                laser.createChild(filter, 0, expected);
                times = 1;
            }};
        }

    }
}
