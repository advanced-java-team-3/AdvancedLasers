import data.LaserColor;
import graphics.LaserPointerWidget;
import graphics.Renderer;
import javafx.geometry.Point3D;
import javafx.scene.image.Image;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import objects.LaserPointer;
import org.junit.Test;

/**
 * Created by shaun on 17/09/2016.
 */
public class LaserPointerWidgetTest {

    @Mocked
    LaserPointer laserPointer;
    @Mocked
    Renderer renderer;
    @Mocked
    Image image;

    @Test
    public void draw() {
        Point3D pos = new Point3D(20, 20, 10);

        new Expectations() {{
            new Image(anyString, false);
            laserPointer.getPosition(); result = pos;
            laserPointer.getColor(); result = LaserColor.RED;
        }};

        LaserPointerWidget widget = new LaserPointerWidget();
        widget.setGameObject(laserPointer);
        widget.draw();

        new Verifications() {{
            renderer.atZIndex(10, (Runnable) any); times = 1;
        }};
    }

}