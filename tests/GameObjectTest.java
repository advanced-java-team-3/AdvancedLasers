import core.Game;
import graphics.Widget;
import javafx.geometry.Point3D;
import javafx.geometry.Rectangle2D;
import javafx.scene.shape.Polygon;
import mockit.Injectable;
import objects.GameObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Feel free to change the behaviour of GameObject as needed, just reflect the
 * changes in these tests.
 * Created by shaun on 24/09/2016.
 */
public class GameObjectTest {

    private GameObject go;

    @Injectable
    private Widget<GameObject> widget;

    @Injectable
    private Game game;

    @Before
    public void setUp() {
        go = new GameObject(game, widget);
    }

    @Test
    public void setPositionChangesBounds() {

        double width = 20;
        double height = 10;
        double halfWidth = Math.round(width/2);
        double halfHeight = Math.round(height/2);

        go.setWidth(20);
        go.setHeight(10);

        double x = 10;
        double y = 5;
        double z = 9;

        go.setPosition(x, y, z);
        Assert.assertEquals(new Polygon(x,y+halfHeight,x+halfWidth,y,x,y-halfHeight,x-halfWidth,y).getPoints(), go.getBounds().getPoints());

        x = 19;
        y = -5;
        z = 0;

        go.setPosition(x, y, z);
        Assert.assertEquals(new Polygon(x,y+halfHeight,x+halfWidth,y,x,y-halfHeight,x-halfWidth,y).getPoints(), go.getBounds().getPoints());
    }

    @Test
    public void setWidthAndHeightChangesBounds() {

        double width = 20;
        double height = 10;
        double halfWidth = Math.round(width/2);
        double halfHeight = Math.round(height/2);

        go.setWidth(width);
        go.setHeight(height);
        // when position is (0, 0)
        Assert.assertEquals(new Polygon(0.0,halfHeight,halfWidth,0.0,0.0,-halfHeight,-halfWidth,0.0).getPoints(), go.getBounds().getPoints());


        width = 5;
        height = 15;

        halfWidth = Math.round(width/2);
        halfHeight = Math.round(height/2);

        go.setWidth(width);
        go.setHeight(height);

        Assert.assertEquals(new Polygon(0.0,halfHeight,halfWidth,0.0,0.0,-halfHeight+1,-halfWidth+1,0.0).getPoints(), go.getBounds().getPoints());
    }

    @Test
    public void setGameObjectAsRectangle() {

        double width = 20;
        double height = 10;
        double halfWidth = Math.round(width/2);
        double halfHeight = Math.round(height/2);

        go.setAsRectangle(width,height);

        // when position is (0, 0)
        Assert.assertEquals(new Polygon(halfWidth,halfHeight,halfWidth,-halfHeight,-halfWidth,-halfHeight,-halfWidth,halfHeight).getPoints(), go.getBounds().getPoints());

    }

    @Test
    public void setWidthAndHeightDoesNotChangePosition() {
        go.setPosition(20, 20, 10);

        go.setWidth(6);
        go.setHeight(4);

        Assert.assertEquals(new Point3D(20, 20, 10), go.getPosition());

        go.setWidth(9);
        go.setHeight(1.25);

        Assert.assertEquals(new Point3D(20, 20, 10), go.getPosition());
    }
}
