# Advanced Lasers #

Advanced Lasers is a 2D puzzle game that deals with the properties of light.

## Downloading ##

You can download the game from [our Pipelines page](https://gitlab.com/advanced-java-team-3/AdvancedLasers/pipelines). Simply select the download button to the right of the latest `master` build.

Once downloaded, extract the zip file and double click on `AdvancedLasers.jar` to run. 

## Main Game ##

The game will open in the main game mode where you can play some pre-made levels. You must complete each level to unlock the next one. Use the "Previous" and "Next" buttons to change levels. The levels that you have unlocked will be saved when you quit so you don't have to unlock them again next time you play.

## Editor ##

The level editor is included with the game, allowing you to create new levels and edit existing ones. Click on level name in the top right to change the name of the level. When you click the save button, it will overwrite any level of the same name or create a new one if it doesn't exist.

Take care when editing the levels included with the game as clicking the save button will overwrite the level. If you want to create a level based on an included level, rename it before saving it.