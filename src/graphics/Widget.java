package graphics;

import core.Game;
import javafx.geometry.Point3D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.transform.Rotate;
import objects.GameObject;

import java.io.Serializable;

/**
 * Draws a game object to the game world.
 * Created by shaun on 16/09/2016.
 */
public abstract class Widget<T extends GameObject> implements Serializable {
    protected T gameObject;
    private double alpha = 1;
    private Light light;

    private boolean missingAsset = false;

    public void setGameObject(T gameObject) {
        this.gameObject = gameObject;
    }

    double getAlpha() {
        return alpha;
    }

    void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public Light getLight() {
        return light;
    }

    void setLight(Light light) {
        this.light = light;
    }

    protected Game getGame() {
            return gameObject.getGame();
    }

    protected GraphicsContext getGraphics() {
        return getGame().getRenderer().getGraphics();
    }

    public abstract void draw();

    void atZIndex(double z, Runnable r) {
        getGame().getRenderer().atZIndex(z, () -> {
            getGraphics().save();

            if (light != null) {
                Lighting lighting = new Lighting(light);
                getGraphics().setEffect(lighting);
            }

            getGraphics().setGlobalAlpha(alpha);

            r.run();

            getGraphics().restore();
        });
    }

    void withRotation(Runnable r) {
        withRotation(gameObject.getRotation(), r);
    }

    private void withRotation(double rotation, Runnable r) {
        withRotation(
                rotation,
                gameObject.getPosition().getX(),
                gameObject.getPosition().getY(),
                r);
    }

    private void withRotation(double rotation,
                                double x,
                                double y,
                                Runnable r) {
        Rotate rotate = new Rotate(rotation, x, y);
        getGraphics().save();
        getGraphics().setTransform(
                rotate.getMxx(),
                rotate.getMyx(),
                rotate.getMxy(),
                rotate.getMyy(),
                rotate.getTx(),
                rotate.getTy());
        r.run();
        getGraphics().restore();
    }

    void drawBounds() {
        drawBounds(gameObject.getPosition());
    }

    void drawBounds(Point3D pos) {
        if(getGame().getDebugMode()) {
            atZIndex(pos.getZ(), () -> {
                getGraphics().setStroke(Color.AQUAMARINE);
                getGraphics().setLineWidth(4);
                getGraphics().strokePolygon(
                        gameObject.getXBounds(),
                        gameObject.getYBounds(),
                        gameObject.getTotalPoints());
            });
        }
    }

    /**
     * Draws the geometry of this widget's game object at the game object's
     * position.
     * @param fill The paint to fill the polygon with.
     * @param stroke The paint to outline the polygon with.
     * @param lineWidth The width of the outline.
     */
    void drawGeometry(Paint fill, Paint stroke, double lineWidth) {
        atZIndex(gameObject.getPosition().getZ(), () -> {
            getGraphics().setFill(fill);
            getGraphics().fillPolygon(
                    gameObject.getXPoints(),
                    gameObject.getYPoints(),
                    gameObject.getTotalPoints());

            getGraphics().setStroke(stroke);
            getGraphics().setLineWidth(lineWidth);
            getGraphics().strokePolygon(
                    gameObject.getXPoints(),
                    gameObject.getYPoints(),
                    gameObject.getTotalPoints());
        });
    }

    /**
     * Called to signify that an asset needed to draw the widget is missing.
     */
    protected void missingAsset() {
        if (!missingAsset) {
            missingAsset = true;
            getGame().onError(
                    "An asset for " + gameObject.getClass().getSimpleName() +
                            " is missing.");
        }
    }
}
