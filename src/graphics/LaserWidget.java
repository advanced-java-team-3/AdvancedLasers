package graphics;

import javafx.geometry.Point3D;
import objects.Laser;

/**
 * Created by Daniel on 9/17/2016.
 */
public class LaserWidget extends Widget<Laser>  {

    /**
     * Draws the laser attached to the widget
     */
    @Override
    public void draw() {
        Point3D pos = gameObject.getPosition();
        if(pos == null) {
            // get a 0,0,0 position if null. Only thing position is being used
            // for is the z cocorinates
            pos = new Point3D(0,0,0);
        }
        atZIndex(pos.getZ(), () -> {
            getGraphics().setStroke(gameObject.getColor().getPaint());
            getGraphics().setLineWidth(4);
            getGraphics().strokeLine(
                    gameObject.getStartPosition().getX(),
                    gameObject.getStartPosition().getY(),
                    gameObject.getEndPosition().getX(),
                    gameObject.getEndPosition().getY());
        });

        drawBounds(pos);
    }
}
