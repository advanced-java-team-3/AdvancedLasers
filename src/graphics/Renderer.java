package graphics;

import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Controls the order in which things are rendered to the game screen.
 * Created by shaun on 17/09/2016.
 */
public class Renderer {
    private TreeMap<Double, ArrayList<Runnable>> drawers
            = new TreeMap<>();

    private GraphicsContext graphics;

    public GraphicsContext getGraphics() {
        return graphics;
    }

    /**
     * Creates a new renderer.
     * @param graphics The graphics context used to draw to the game screen.
     */
    public Renderer(GraphicsContext graphics) {
        this.graphics = graphics;
    }

    /**
     * Executes the specified action, such that all draw calls will be made at
     * the specified z position (i.e. layer) on the screen.
     * @param index The z position, or layer to draw objects at.
     * @param runnable The runnable containing draw calls to be drawn at the z
     *                 position.
     */
    public void atZIndex(double index, Runnable runnable) {
        drawers.putIfAbsent(index, new ArrayList<>());
        drawers.get(index).add(runnable);
    }

    /**
     * Executes the stored runnables so that each one draws its objects at the
     * correct z position.
     */
    public void draw() {
        for (Map.Entry<Double, ArrayList<Runnable>> d : drawers.entrySet()) {
            d.getValue().forEach(Runnable::run);
        }
        drawers.clear();
    }
}