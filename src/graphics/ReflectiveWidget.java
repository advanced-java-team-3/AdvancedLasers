package graphics;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import objects.ReflectiveObject;

/**
 * Created by Daniel on 10/1/2016.
 */
public class ReflectiveWidget extends Widget<ReflectiveObject> {

    /**
     * Draws the reflective object (mirror) attached to the widget
     */
    @Override
    public void draw() {
        Stop[] stops = new Stop[] { new Stop(0, Color.GRAY), new Stop(1, Color.LIGHTGRAY)};
        LinearGradient lg1 = new LinearGradient(0, 0, 0.5, 0.5, true, CycleMethod.REFLECT, stops);
        drawGeometry(lg1, Color.LIGHTGRAY, 2);

        drawBounds();
    }
}
