package graphics;

import core.Assets;
import javafx.geometry.Point3D;
import javafx.scene.image.Image;
import objects.LaserPointer;

/**
 * Created by shaun on 16/09/2016.
 */
public class LaserPointerWidget extends Widget<LaserPointer> {

    /**
     * Draws the laser pointer attached to the widget
     */
    @Override
    public void draw() {
        Image pointer = getImage();

        if (pointer == null)
            return;

        Point3D pos = gameObject.getPosition();

        atZIndex(pos.getZ(), () -> {
            withRotation(() -> getGraphics().drawImage(pointer,
                    pos.getX() - (pointer.getWidth()/2),
                    pos.getY() - (pointer.getHeight()/2)));
        });
        drawBounds(pos);
    }

    private Image getImage() {
        String pre = "laser_pointer_";
        String name = pre + gameObject.getColor().toString();
        Image image =  Assets.getImage(name);

        if (image == null)
            missingAsset();

        return image;
    }
}
