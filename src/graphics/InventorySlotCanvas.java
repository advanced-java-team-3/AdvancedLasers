package graphics;

import core.Input;
import core.Inventory;
import core.InventorySlot;
import data.ALObject;
import data.InputCommand;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Created by Timothy on 10/10/2016.
 */
public class InventorySlotCanvas {
    private Canvas canvas;
    private Inventory inv;
    private InventorySlot slot;
    private Input input;

    private Image image;

    public InventorySlotCanvas(InventorySlot slot, Inventory inv, Input input) {
        this.slot = slot;
        this.inv = inv;
        this.input = input;
        canvas = new Canvas(40, 40);

        String imagePath = ALObject.getImagePath(slot.getAlObjectType());

        try {
            image = new Image("/" + imagePath);
        }
        catch (IllegalArgumentException e) {
            image = null;
        }

        updateCanvas();
        setCanvasEvent();
    }

    /**
     * Updates the canvas of the individual inventory slot
     */
    private void updateCanvas() {
        GraphicsContext gc = canvas.getGraphicsContext2D();

        if (slot.isSelected()) {
            gc.setFill(Color.LIGHTGRAY);
        }
        else {
            gc.setFill(Color.WHITE);
        }
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        if (image != null) {
            gc.drawImage(image, 0, 0, 40, 40);
        }

        gc.setFill(Color.DARKCYAN);
        gc.setFont(Font.font(null, FontWeight.BOLD, 12));
        gc.fillText("" + slot.getCurrentAmount() + "/" + slot.getMaxAmount(), 2.0, 38.0);

        input.requestCommand(InputCommand.UPDATE_UI);
    }

    /**
     * Sets the on mouse click event for the canvas
     */

    private void setCanvasEvent() {
        canvas.setOnMouseClicked(e -> {
            inv.selectSlot(slot);
            updateCanvas();
        });
    }

    public Canvas getCanvas() {
        return canvas;
    }
}
