package graphics;

import javafx.scene.paint.Color;;
import javafx.scene.paint.Paint;
import objects.Target;

/**
 * Created by Daniel on 10/15/2016.
 */
public class TargetWidget extends Widget<Target> {

    /**
     * Draws the target attached to the widget
     */
    @Override
    public void draw() {
        if(gameObject.isActive()) {
            drawTarget(gameObject.getColor().getPaint(), Color.LIGHTGRAY);
        }
        else
        {
            drawTarget(gameObject.getColor().getPaint(), Color.BLACK);
        }
        drawBounds();
    }

    /**
     * Draws a target with certain fill and sroke colors
     * @param fill The paint color for the targets donut centers
     * @param stroke The paint color for the targets edges
     */
    private void drawTarget(Paint fill, Paint stroke)
    {
        atZIndex(gameObject.getPosition().getZ(), () -> {
            getGraphics().setFill(fill);
            getGraphics().fillPolygon(
                    gameObject.getXPoints(),
                    gameObject.getYPoints(),
                    gameObject.getTotalPoints());

            getGraphics().setStroke(stroke);
            getGraphics().setLineWidth(2);
            getGraphics().strokeOval(
                    gameObject.getPosition().getX()- (gameObject.getWidth()/2),
                    gameObject.getPosition().getY()- (gameObject.getHeight()/2),
                    gameObject.getWidth(),
                    gameObject.getHeight());

            getGraphics().setStroke(stroke);
            getGraphics().setLineWidth(4);
            getGraphics().strokeOval(
                    gameObject.getPosition().getX()- (gameObject.getWidth()/4),
                    gameObject.getPosition().getY()- (gameObject.getHeight()/4),
                    gameObject.getWidth()/2,
                    gameObject.getHeight()/2);
        });
    }
}