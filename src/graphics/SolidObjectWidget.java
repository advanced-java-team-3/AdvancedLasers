package graphics;

import javafx.scene.paint.Color;
import objects.SolidObject;

/**
 * Created by Daniel on 9/21/2016.
 */
public class SolidObjectWidget extends Widget<SolidObject> {

    /**
     * Draws the solid object (generic) attached to the widget
     */
    @Override
    public void draw() {
        drawGeometry(Color.TRANSPARENT, Color.BLACK, 4);
        drawBounds();
    }
}
