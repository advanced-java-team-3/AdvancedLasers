package graphics;

import data.Options;
import javafx.geometry.Point3D;
import javafx.scene.effect.Light;
import javafx.scene.paint.Color;
import objects.Placeholder;

/**
 * Renders the game object contained in a placeholder.
 * Created by shaun on 1/10/2016.
 */
public class PlaceholderWidget extends Widget<Placeholder> {

    private Widget<?> widget;
    private double originalAlpha;
    private double originalZ;

    public PlaceholderWidget(Widget<?> widget) {
        this.widget = widget;
        originalAlpha = widget.getAlpha();
        Point3D pos = widget.gameObject.getPosition();
        originalZ = pos.getZ();
        widget.setAlpha(Options.DRAG_ALPHA);
    }

    /**
     * Restores the transparency and z coordinate of the widget and game object
     * contained in the placeholder.
     */
    public void restore() {
        widget.setAlpha(originalAlpha);
        Point3D pos = gameObject.getPosition();
        widget.setLight(null);
        widget.gameObject.setPosition(pos.getX(), pos.getY(), originalZ);
    }

    /**
     * Draws the placeholder object attached to the widget
     */
    @Override
    public void draw() {
        if (gameObject.isColliding()) {
            Light.Distant light = new Light.Distant();
            light.setColor(Color.rgb(255, 127, 127));
            widget.setLight(light);
        }
        else {
            widget.setLight(null);
        }

        widget.draw();
    }
}
