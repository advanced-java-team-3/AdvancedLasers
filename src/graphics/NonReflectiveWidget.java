package graphics;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import objects.NonReflectiveObject;

/**
 * Created by Daniel on 10/1/2016.
 */
public class NonReflectiveWidget extends Widget<NonReflectiveObject> {

    /**
     * Draws the non reflective object (block of wood) attached to the widget
     */
    @Override
    public void draw() {
        Stop[] stops = new Stop[] { new Stop(0, Color.SANDYBROWN), new Stop(1, Color.SADDLEBROWN)};
        LinearGradient lg1 = new LinearGradient(0, 0, 0.5, 0.5, true, CycleMethod.REFLECT, stops);
        drawGeometry(lg1, Color.SADDLEBROWN, 2);

        drawBounds();
    }
}
