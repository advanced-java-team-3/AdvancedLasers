package graphics;

import javafx.scene.paint.Color;
import objects.Filter;

/**
 * Created by shaun on 6/10/2016.
 */
public class FilterWidget extends Widget<Filter> {

    /**
     * Draws the filter attached to the widget
     */
    @Override
    public void draw() {
        drawGeometry(gameObject.getColor().getPaint(), Color.BLACK, 2);
        drawBounds();
    }
}
