package data;

import javafx.geometry.Point3D;

import java.io.Serializable;

/**
 * A point or vector with an x, y and z component.
 * Created by Daniel on 10/7/2016.
 */
public class Point3DSerializable implements Serializable  {
    private double xPosition;
    private double yPosition;
    private double ZPosition;

    public Point3DSerializable(double x,double y,double z) {
        setPoint3D(x,y,z);
    }

    public Point3D getPoint3D() {
        return new Point3D(xPosition,yPosition,ZPosition);
    }

    private void setPoint3D(double x,double y,double z) {
        xPosition = x;
        yPosition = y;
        ZPosition = z;
    }

    public void setPoint3D(Point3D point) {
        xPosition = point.getX();
        yPosition = point.getY();
        ZPosition = point.getZ();
    }
}
