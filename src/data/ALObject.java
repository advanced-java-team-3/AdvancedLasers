package data;

import core.Game;
import objects.*;

/**
 * Represents a specific type of game object in the inventory.
 * Created by Timothy on 10/10/2016.
 */
public enum ALObject {
    FLAT_MIRROR,
    WOOD_BLOCK,
    RED_LASER_POINTER,
    GREEN_LASER_POINTER,
    BLUE_LASER_POINTER,
    CYAN_LASER_POINTER,
    MAGENTA_LASER_POINTER,
    YELLOW_LASER_POINTER,
    WHITE_LASER_POINTER,
    RED_FILTER,
    GREEN_FILTER,
    BLUE_FILTER,
    CYAN_FILTER,
    MAGENTA_FILTER,
    YELLOW_FILTER,
    WHITE_FILTER,
    RED_TARGET,
    GREEN_TARGET,
    BLUE_TARGET,
    CYAN_TARGET,
    MAGENTA_TARGET,
    YELLOW_TARGET,
    WHITE_TARGET;

    /**
     * Creates a game object based on an ALObject
     *
     * @param type AlObject that gameobject should be based on
     * @param game The game as a reference for the final gameObject
     * @return A final gameObject
     */
    public static GameObject create(ALObject type, Game game) {
        GameObject toReturn;

        // Mirror options
        double mirrorWidth = 40.0;
        double mirrorHeight = 10.0;
        int mirrorSides = 4;
        double mirrorRotation = -45.0;

        // Woodblock options
        double woodWidth = 39.0;
        double woodHeight = 39.0;
        int woodSides = 4;
        double woodRotation = 0.0;

        // Filter options
        double filterWidth = 20.0;
        double filterHeight = 40.0;
        int filterSides = 8;
        double filterRotation = 0.0;

        // Target options
        double targetRadius = 20.0;

        switch (type) {
            case FLAT_MIRROR:
                toReturn = new ReflectiveObject(game, mirrorWidth, mirrorHeight,
                        mirrorSides, mirrorRotation);
                toReturn.setAsRectangle(mirrorWidth, mirrorHeight);
                break;
            case WOOD_BLOCK:
                toReturn = new NonReflectiveObject(game, woodWidth, woodHeight,
                        woodSides, woodRotation);
                toReturn.setAsRectangle(woodWidth, woodHeight);
                break;
            case RED_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.RED);
                break;
            case GREEN_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.GREEN);
                break;
            case BLUE_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.BLUE);
                break;
            case CYAN_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.CYAN);
                break;
            case MAGENTA_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.MAGENTA);
                break;
            case YELLOW_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.YELLOW);
                break;
            case WHITE_LASER_POINTER:
                toReturn = new LaserPointer(game, LaserColor.WHITE);
                break;
            case RED_FILTER:
                toReturn = new Filter(game, LaserColor.RED,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case GREEN_FILTER:
                toReturn = new Filter(game, LaserColor.GREEN,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case BLUE_FILTER:
                toReturn = new Filter(game, LaserColor.BLUE,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case CYAN_FILTER:
                toReturn = new Filter(game, LaserColor.CYAN,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case MAGENTA_FILTER:
                toReturn = new Filter(game, LaserColor.MAGENTA,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case YELLOW_FILTER:
                toReturn = new Filter(game, LaserColor.YELLOW,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case WHITE_FILTER:
                toReturn = new Filter(game, LaserColor.WHITE,
                        filterWidth, filterHeight, filterSides, filterRotation);
                break;
            case RED_TARGET:
                toReturn = new Target(game, LaserColor.RED);
                toReturn.setAsCircle(targetRadius);
                break;
            case BLUE_TARGET:
                toReturn = new Target(game, LaserColor.BLUE);
                toReturn.setAsCircle(targetRadius);
                break;
            case GREEN_TARGET:
                toReturn = new Target(game, LaserColor.GREEN);
                toReturn.setAsCircle(targetRadius);
                ;
                break;
            case CYAN_TARGET:
                toReturn = new Target(game, LaserColor.CYAN);
                toReturn.setAsCircle(targetRadius);
                break;
            case YELLOW_TARGET:
                toReturn = new Target(game, LaserColor.YELLOW);
                toReturn.setAsCircle(targetRadius);
                break;
            case MAGENTA_TARGET:
                toReturn = new Target(game, LaserColor.MAGENTA);
                toReturn.setAsCircle(targetRadius);
                break;
            case WHITE_TARGET:
                toReturn = new Target(game, LaserColor.WHITE);
                toReturn.setAsCircle(targetRadius);
                break;

            default:
                toReturn = new LaserPointer(game, LaserColor.WHITE);
        }

        toReturn.setObjectType(type);
        return toReturn;
    }

    /**
     * Gets the image path of a specific ALObject
     *
     * @param type The AlObject
     * @return STring of the image path
     */
    public static String getImagePath(ALObject type) {
        String toReturn = "";
        switch (type) {
            case FLAT_MIRROR:
                toReturn += "inv_flat_mirror.png";
                break;
            case WOOD_BLOCK:
                toReturn += "inv_wb.png";
                break;
            case RED_LASER_POINTER:
                toReturn += "inv_lp_red.png";
                break;
            case GREEN_LASER_POINTER:
                toReturn += "inv_lp_green.png";
                break;
            case BLUE_LASER_POINTER:
                toReturn += "inv_lp_blue.png";
                break;
            case CYAN_LASER_POINTER:
                toReturn += "inv_lp_cyan.png";
                break;
            case MAGENTA_LASER_POINTER:
                toReturn += "inv_lp_magenta.png";
                break;
            case YELLOW_LASER_POINTER:
                toReturn += "inv_lp_yellow.png";
                break;
            case WHITE_LASER_POINTER:
                toReturn += "inv_lp_white.png";
                break;
            case RED_FILTER:
                toReturn += "inv_f_red.png";
                break;
            case GREEN_FILTER:
                toReturn += "inv_f_green.png";
                break;
            case BLUE_FILTER:
                toReturn += "inv_f_blue.png";
                break;
            case CYAN_FILTER:
                toReturn += "inv_f_cyan.png";
                break;
            case MAGENTA_FILTER:
                toReturn += "inv_f_magenta.png";
                break;
            case YELLOW_FILTER:
                toReturn += "inv_f_yellow.png";
                break;
            case WHITE_FILTER:
                toReturn += "inv_f_white.png";
                break;
            case BLUE_TARGET:
                toReturn += "inv_t_blue.png";
                break;
            case RED_TARGET:
                toReturn += "inv_t_red.png";
                break;
            case GREEN_TARGET:
                toReturn += "inv_t_green.png";
                break;
            case YELLOW_TARGET:
                toReturn += "inv_t_yellow.png";
                break;
            case MAGENTA_TARGET:
                toReturn += "inv_t_magenta.png";
                break;
            case CYAN_TARGET:
                toReturn += "inv_t_cyan.png";
                break;
            case WHITE_TARGET:
                toReturn += "inv_t_white.png";
                break;

            default:
                toReturn += "";
                break;
        }
        return toReturn;
    }
}
