package data;

import javafx.scene.paint.Color;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Represents the color a laser, laser pointer or filter.
 * Created by shaun on 16/09/2016.
 */
public enum LaserColor {
    RED,
    GREEN,
    BLUE,
    CYAN,
    MAGENTA,
    YELLOW,
    WHITE;

    private static HashMap<Color, LaserColor> paintMap = new HashMap<>();
    private static Random random = new Random();

    static {
        RED.paint = Color.rgb(255, 0, 0);
        RED.name = "red";
        GREEN.paint = Color.rgb(0, 255, 0);
        GREEN.name = "green";
        BLUE.paint = Color.rgb(0, 0, 255);
        BLUE.name = "blue";
        CYAN.paint = Color.rgb(0, 255, 255);
        CYAN.name = "cyan";
        MAGENTA.paint = Color.rgb(255, 0, 255);
        MAGENTA.name = "magenta";
        YELLOW.paint = Color.rgb(255, 255, 0);
        YELLOW.name = "yellow";
        WHITE.paint = Color.rgb(255, 255, 255);
        WHITE.name = "white";

        for (LaserColor c : values()) {
            paintMap.put(c.paint, c);
        }
    }

    /**
     * Convert a JavaFX color to a laser color.
     * @param color the JavaFX color to convert.
     * @return the laser color associated with the specified JavaFX color.
     */
    public static LaserColor fromPaint(Color color) {
        return paintMap.get(color);
    }

    /**
     * Returns a random laser color based on available laser color values
     * @return Random LaserColor
     */
    public static LaserColor random() {
        int i = random.nextInt(LaserColor.values().length);
        return LaserColor.values()[i];
    }

    private Color paint;
    private String name;

    /**
     * Convert a laser color to a JavaFX color.
     * @return the JavaFX color associated with this laser color.
     */
    public Color getPaint() {
        return paint;
    }

    public boolean isPrimary() {
        return Stream.of(RED, GREEN, BLUE).anyMatch(c -> c == this);
    }

    public boolean isSecondary() {
        return this != WHITE && !isPrimary();
    }

    /**
     * Determines if this is a component primary color of the specified
     * secondary color.
     * @param other The secondary color to test.
     * @return <code>true</code> if this is a component primary color,
     * <code>false</code> otherwise.
     */
    public boolean isComponentOf(LaserColor other) {
        if (this == RED)
            return Stream.of(MAGENTA, YELLOW).anyMatch(c -> c == other);

        if (this == GREEN)
            return Stream.of(YELLOW, CYAN).anyMatch(c -> c == other);

        if (this == BLUE)
            return Stream.of(CYAN, MAGENTA).anyMatch(c -> c == other);

        return false;
    }

    /**
     * Determines the shared primary color of this and another secondary colour.
     * @param other The other secondary colour.
     * @return The shared component primary color, or <code>null</code> if these
     * aren't secondary colors, or if they are the same color.
     */
    public LaserColor getShared(LaserColor other) {
        if (this == other)
            return null;

        HashSet<LaserColor> s = new HashSet<>();
        s.add(this);
        s.add(other);

        if (s.contains(CYAN) && s.contains(MAGENTA))
            return BLUE;

        if (s.contains(MAGENTA) && s.contains(YELLOW))
            return RED;

        if (s.contains(YELLOW) && s.contains(CYAN))
            return GREEN;

        return null;
    }

    @Override
    public String toString() {
        return name;
    }
}
