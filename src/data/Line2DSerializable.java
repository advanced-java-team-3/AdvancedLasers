package data;

import java.awt.geom.Line2D;
import java.io.Serializable;

/**
 * A 2 dimensional line with a start and end point.
 * Created by Daniel on 10/7/2016.
 */
public class Line2DSerializable implements Serializable  {
    private Point2DSerializable point1;
    private Point2DSerializable point2;

    public Line2DSerializable(double x1, double y1, double x2, double y2) {
        setLine2D(x1,y1,x2,y2);
    }

    public Line2DSerializable(Line2D.Double line) {
        setLine2D(line.getX1(),line.getY1(),line.getX2(),line.getY2());
    }

    public Line2D.Double getLine2D() {
        return new Line2D.Double(point1.getPoint2D().getX(),point1.getPoint2D().getY(),point2.getPoint2D().getX(),point2.getPoint2D().getY());
    }

    private void setLine2D(double x1, double y1, double x2, double y2) {
        point1 = new Point2DSerializable(x1,y1);
        point2 = new Point2DSerializable(x2,y2);
    }
}