package data;

import javafx.geometry.Point2D;

import java.io.Serializable;

/**
 * A point or vector with a x and y component.
 * Created by Daniel on 10/7/2016.
 */
public class Point2DSerializable implements Serializable  {
    private double xPosition;
    private double yPosition;

    public Point2DSerializable(double x,double y)
    {
        setPoint2D(x,y);
    }

    public Point2DSerializable(Point2D point) {
        setPoint2D(point.getX(),point.getY());
    }

    public Point2D getPoint2D()
    {
        return new Point2D(xPosition,yPosition);
    }

    private void setPoint2D(double x,double y) {
        xPosition = x;
        yPosition = y;
    }
}
