package data;

import javafx.scene.paint.Color;

/**
 * Constants related to the game.
 * Created by shaun on 24/09/2016.
 */
public class Options {
    public static final int SCREEN_WIDTH = 1280;
    public static final int SCREEN_HEIGHT = 720;
    public static final int GAME_WIDTH = 720;
    public static final int GAME_HEIGHT = 720;
    /**
     * Background color of the game area.
     */
    public static final Color BACKGROUND_COLOR = Color.rgb(64, 52, 71);
    /**
     * Color of the grid lines in the game area.
     */
    public static final Color GRID_LINE_COLOR = Color.rgb(123, 115, 128);
    /**
     * Thickness of the grid lines.
     */
    public static final double GRID_LINE_WIDTH = 1.0;
    /**
     * Length of the grid line dashes.
     */
    public static final double GRID_LINE_DASH = 2.0;
    /**
     * Amount of spacing between the dashes.
     */
    public static final double GRID_LINE_DASH_SPACING = 4.0;
    /**
     * The distance in pixels between game object snap points on the x-axis
     */
    public static final double GRID_X_SNAP = 32;
    /**
     * The distance in pixels between game object snap points on the y-axis
     */
    public static final double GRID_Y_SNAP = 32;
    /**
     * The distance in pixels that a single laser will travel in any direction
     */
    public static final int MAX_LASER_DISTANCE = 4000;
    /**
     * The maximum individual lasers that will be generated from a single laser pointer
     */
    public static final int MAX_LASER_LENGTH = 100;
    /**
     * The level of transparency a widget has when being moved by the player.
     */
    public static final double DRAG_ALPHA = 0.5;
    /**
     * The position on the z axis that widgets should be drawn when being moved
     * by the player.
     */
    public static final double DRAG_Z = 1000;
    /**
     * String used to get the folder location,
     * relative to the project, of all the levels
     */
    public static final String LEVEL_FILES = "levels/";
    /**
     * String used to get the save location,
     * relative to the project, of all the player saves
     */
    public static final String SAVE_FILES = "saves/";
}
