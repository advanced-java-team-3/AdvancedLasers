package data;

/**
 * Represents what action that the user wants done.
 * Created by Timothy on 26/09/2016.
 */
public enum InputCommand {
    CLEAR, DEBUG,
    MAINGAME, EDITOR,
    SAVE_CURRENT,
    LOAD_NEXT_LEVEL, LOAD_PREVIOUS_LEVEL,
    INCREASE_INVENTORY, DECREASE_INVENTORY,
    UPDATE_UI
}


