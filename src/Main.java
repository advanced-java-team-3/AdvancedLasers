import core.Engine;
import data.Options;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import ui.GameUI;

/**
 * Entry point into the application.
 * Initializes the user interface and engine and starts the main game loop.
 * Created by shaun on 14/09/2016.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();

        Canvas canvas = new Canvas(Options.GAME_WIDTH, Options.GAME_HEIGHT);
        GameUI gameUI = new GameUI(root, primaryStage, Options.SCREEN_WIDTH, Options.SCREEN_HEIGHT, canvas);

        primaryStage.setScene(gameUI);
        primaryStage.show();

        Engine engine = new Engine(canvas, canvas.getGraphicsContext2D());
        gameUI.setGame(engine.getGame());

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                engine.tick(now);
            }
        };

        timer.start();
    }
}
