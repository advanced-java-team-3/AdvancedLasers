package objects;

import core.Game;
import graphics.ReflectiveWidget;

import java.awt.geom.Line2D;

/**
 * A game object that causes light that hits it to bounce in a direction.
 * Created by Daniel on 9/24/2016.
 */
public class ReflectiveObject extends SolidObject {
    public ReflectiveObject(Game game)
    {
        super(game, new ReflectiveWidget());
    }

    public ReflectiveObject(Game game, double width, double height, int sides, double rotation) {
        super(game, new ReflectiveWidget(), width, height, sides, rotation);
    }

    /**
     * OnLaser hit override for the ReflectiveObject creates a new laser line
     * and reflects it off the polygon lines surface
     * @param laser The laser segment that hit this object.
     * @param edgeHit The edge of this object's geometry that was hit.
     */

    @Override
    public void onLaserHit(Laser laser, Line2D.Double edgeHit) {
        double angleInRadians = Math.toRadians(laser.getRotation());

        // get the angle in radians for the normal
        double deltaX = edgeHit.getP2().getX() - edgeHit.getP1().getX();
        double deltaY = edgeHit.getP2().getY() - edgeHit.getP1().getY();
        // yes the parameters are deltaX then -deltaY
        double normal = Math.atan2(deltaX, -deltaY);

        // get the new direction based on the old angle and the normal (whatever
        // normal it is)
        double angleDirrerence = normal - (angleInRadians- Math.PI);
        double reflectionResult = normal + (angleDirrerence);

        double reflectionResultDegrees = Math.toDegrees(reflectionResult);

        laser.createChild(this, reflectionResultDegrees, laser.getColor());
    }
}
