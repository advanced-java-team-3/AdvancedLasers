package objects;

import core.Game;
import graphics.Widget;

import java.awt.geom.Line2D;
import java.util.Random;

/**
 * The base class for game objects that somehow alter the path of a laser.
 * Created by Daniel on 9/24/2016.
 */
public class SolidObject extends GameObject {
    private Random random = new Random();

    public SolidObject(Game game, Widget widget) {
        super(game, widget);
        setTotalPoints(getRandomAngle());
        setWidth(getRandomWidth());
        setHeight(getRandomHeight());
        setRotation(0);
    }

    public SolidObject(Game game, Widget widget, double width, double height, int sides, double rotation) {
        super(game, widget);
        setTotalPoints(sides);
        setWidth(width);
        setHeight(height);
        setRotation(rotation);
    }

    /**
     * Called when a laser segment hits this object.
     * Inheriting classes should override this to provide functionality,
     * otherwise this will act like a wall and block the laser.
     *
     * @param laser   The laser segment that hit this object.
     * @param edgeHit The edge of this object's geometry that was hit.
     */
    public void onLaserHit(Laser laser, Line2D.Double edgeHit) {

    }

    //the following functions are just used to create random values for testing

    public int getRandomAngle() {
        return random.nextInt(15) + 3;
    }

    public int getRandomWidth() {
        return random.nextInt(40) + 20;
    }

    public int getRandomHeight() {
        return random.nextInt(40) + 20;
    }
}
