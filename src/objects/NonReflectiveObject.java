package objects;

import core.Game;
import graphics.NonReflectiveWidget;

/**
 * A game object that blocks and absorbs light that hits it.
 * Created by Daniel on 9/24/2016.
 */
public class NonReflectiveObject  extends SolidObject
{
    public NonReflectiveObject(Game game) {
        super(game, new NonReflectiveWidget());
    }

    public NonReflectiveObject(Game game, double width, double height, int sides, double rotation) {
        super(game, new NonReflectiveWidget(), width, height, sides, rotation);
    }
}