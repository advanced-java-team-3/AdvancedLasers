package objects;

import core.Game;
import data.LaserColor;
import data.Options;
import graphics.LaserPointerWidget;
import javafx.geometry.Point3D;

/**
 * A game object that emits a beam of coloured light in a direction.
 * Created by shaun on 16/09/2016.
 */
public class LaserPointer extends GameObject {

    private Laser childLaser;

    public LaserPointer(Game game, LaserColor c) {
        super(game, new LaserPointerWidget());
        setColor(c);
        setAsRectangle(64,49);
    }

    @Override
    public void update() {
        recalculate();
    }

    /**
     * Recalculates the laser physics by creating a new laser child
     * and discarding the old one
     */
    private void recalculate() {
        Point3D pos = new Point3D(
                getPosition().getX(),
                getPosition().getY(),
                0);

        childLaser = new Laser(
                game,
                pos,
                getRotation(),
                this,
                Options.MAX_LASER_LENGTH,
                getColor());
    }

    /**
     * The draw method of the Laser Pointer.
     * The laser pointer draws itself and calls the draw method of its
     * laser child if it has one
     */
    @Override
    public void draw() {
        widget.draw();
        if (childLaser != null) {
            childLaser.draw();
        }
    }
}
