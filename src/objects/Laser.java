package objects;

import core.Game;
import data.LaserColor;
import data.Line2DSerializable;
import data.Options;
import data.Point2DSerializable;
import graphics.LaserWidget;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * A straight beam of light that travels in a certain direction.
 * A laser can be in a chain of a number of other lasers which make up the path
 * that light emitted from a laser pointer travels.
 * Created by Daniel on 9/17/2016.
 */
public class Laser extends GameObject {

    private Point2DSerializable startPosition;
    private Point2DSerializable endPosition;
    private Point2DSerializable confirmedIntersectionPoint;
    private Line2DSerializable laserLine;
    private Line2DSerializable otherLine;
    private GameObject otherObject;
    private Laser childLaser;
    private int laserDecay;

    private Laser(Game game, double x, double y, double direction, GameObject sourceObject, int decay, LaserColor c) {
        super(game, new LaserWidget());
        setColor(c);
        startPosition = new Point2DSerializable(x,y);
        laserDecay = decay;
        setRotation(direction);
        childLaser = null;
        extendLaser(sourceObject);
    }

    Laser(Game game, Point3D sPosition, double direction, GameObject sourceObject, int decay, LaserColor c) {
        super(game, new LaserWidget());
        setColor(c);
        startPosition = new Point2DSerializable(sPosition.getX(),sPosition.getY());
        laserDecay = decay;
        setRotation(direction);
        childLaser = null;
        extendLaser(sourceObject);
    }

    /**
     * Creates a new laser segment originating from the end of this segment.
     * @param sourceObject The object that caused the laser segment to be added.
     * @param direction The rotation in degrees of the new laser segment.
     * @param color The color of the new laser segment.
     * @return the newly created laser segment, or <code>null</code> if the
     * laser has reached its maximum limit of segments.
     */
    public Laser createChild(GameObject sourceObject,
                             double direction,
                             LaserColor color) {
        if (laserDecay < 1)
            return null;

        otherObject = sourceObject;
        childLaser = new Laser(
                game,
                laserLine.getLine2D().getP2().getX(),
                laserLine.getLine2D().getP2().getY(),
                direction,
                sourceObject,
                laserDecay - 1,
                color);
        return childLaser;
    }

    /**
     * Extends the laser object from its source object.
     * Extends the laser to its max length, then checks if it
     * collides with any solid objects. If it does execute onHit on the other
     * object at the intersection point.
     * @param sourceObject The source of the laser
     *                     (either laser pointer or another laser)
     */
    private void extendLaser(GameObject sourceObject) {
        //convert direction to radians
        double angleInRadians = Math.toRadians(getRotation());

        //find the maximum position to draw the line between,
        double endX = startPosition.getPoint2D().getX() + Options.MAX_LASER_DISTANCE * Math.cos(angleInRadians);
        double endY = startPosition.getPoint2D().getY() + Options.MAX_LASER_DISTANCE * Math.sin(angleInRadians);
        endX = Math.round(endX);
        endY = Math.round(endY);
        endPosition = new Point2DSerializable(endX,endY);
        laserLine = new Line2DSerializable(startPosition.getPoint2D().getX(),startPosition.getPoint2D().getY(),endPosition.getPoint2D().getX(),endPosition.getPoint2D().getY());

        confirmedIntersectionPoint = null;
        otherLine = null;
        //do a collision check with the line
        for (GameObject o : game.getGameObjects()) {
            //if the object is a solid object and not the source of this laser
            if (o instanceof SolidObject && o != sourceObject) {
                checkObject((SolidObject) o);
            }
        }

        //set end x & y to the new collision position
        if(confirmedIntersectionPoint != null) {
            endX = confirmedIntersectionPoint.getPoint2D().getX();
            endY = confirmedIntersectionPoint.getPoint2D().getY();
            endPosition = new Point2DSerializable(endX,endY);
            laserLine = new Line2DSerializable(startPosition.getPoint2D().getX(),startPosition.getPoint2D().getY(),
                    endPosition.getPoint2D().getX(),endPosition.getPoint2D().getY());
        }

        //figure out the new direction if it reflects
        if(otherLine != null && confirmedIntersectionPoint != null
                && otherObject != null)
        {
            SolidObject solidObject = (SolidObject) otherObject;
            solidObject.onLaserHit(this, otherLine.getLine2D());
        }
    }

    /**
     * Checks if the laser intersects with a specific object
     * Creates a list of lines based on the polygon edges of the object
     * Then sorts the list with closest lines first and checks each line until
     * it finds an intersection point
     * @param object The objects it may interact with
     */
    private void checkObject(SolidObject object) {
        double[] xPoints = object.getXPoints();
        double[] yPoints = object.getYPoints();
        int sides = object.getTotalPoints();
        List<Line2D.Double> listOfLines = new ArrayList<>();

        //create all lines
        for(int i = 0; i < sides; i++)
        {
            if(i<sides-1) {
                listOfLines.add(new Line2D.Double(xPoints[i], yPoints[i], xPoints[i + 1], yPoints[i + 1]));
            }
            else
            {
                //line between the first and last poits
                listOfLines.add(new Line2D.Double(xPoints[i], yPoints[i], xPoints[0], yPoints[0]));
            }
        }

        //sort all lines, with lines closest to the pointer origin first
        Collections.sort(listOfLines, createComparator(startPosition.getPoint2D()));

        //check all lines
        for(int i = 0; i < listOfLines.size(); i++)
        {
            checkIntersection(listOfLines.get(i),object);
        }
    }

    /**
     * Creates a comparator class between two lines that softs them by distance
     * from a point
     * @param p The point the lines are sorted by
     * @return The compared distance between both lines and the point
     */
    private static Comparator<Line2D.Double> createComparator(Point2D p) {
        final Point2D finalP = new Point2D(p.getX(), p.getY());
        return new Comparator<Line2D.Double>()
        {
            @Override
            public int compare(Line2D.Double l0, Line2D.Double l1)
            {
                Point2D midPoint0 = new Point2D(
                        l0.getP1().getX() - ((l0.getP1().getX()-l0.getP2().getX())/2),
                        l0.getP1().getY() - ((l0.getP1().getY()-l0.getP2().getY())/2));
                Point2D midPoint1 = new Point2D(
                        l1.getP1().getX() - ((l1.getP1().getX()-l1.getP2().getX())/2),
                        l1.getP1().getY() - ((l1.getP1().getY()-l1.getP2().getY()))/2);
                double ds0 = midPoint0.distance(finalP);
                double ds1 = midPoint1.distance(finalP);
                return Double.compare(ds0, ds1);
            }
        };
    }

    /**
     * Checks if the laser will intersect with a line
     * @param line The line to check if there is an intersection
     * @param object The object that contains the polygon points
     *               the line was created with
     */
    private void checkIntersection(Line2D.Double line, SolidObject object) {
        Point2D temporaryIntersectionPoint = getIntersection(laserLine.getLine2D(), line);

        //if a new line has made the laser shorter (or at least different)
        if((temporaryIntersectionPoint.getX() != laserLine.getLine2D().getP2().getX() ||
                temporaryIntersectionPoint.getY() != laserLine.getLine2D().getP2().getY()))
        {
            confirmedIntersectionPoint = new Point2DSerializable(temporaryIntersectionPoint);
            otherLine = new Line2DSerializable(line);
            otherObject = object;
            laserLine = new Line2DSerializable(laserLine.getLine2D().getP1().getX(),laserLine.getLine2D().getP1().getY(),
                    temporaryIntersectionPoint.getX(),temporaryIntersectionPoint.getY());
        }
    }

    /**
     * Gets the intersection point between two lines
     * @param laserLine The line belonging to the laser
     * @param otherLine The line belonging to the object
     * @return A point 2D of a new intersection point OR the end point of the
     * line if there is no intersection point
     */
    private Point2D getIntersection(Line2D.Double laserLine, Line2D.Double otherLine) {
        double x1,y1, x2,y2, x3,y3, x4,y4;
        x1 = laserLine.x1; y1 = laserLine.y1; x2 = laserLine.x2; y2 = laserLine.y2;
        x3 = otherLine.x1; y3 = otherLine.y1; x4 = otherLine.x2; y4 = otherLine.y2;
        //gets the intersection, assuming that both lines are infinite length
        double x = (
                (x2 - x1)*(x3*y4 - x4*y3) - (x4 - x3)*(x1*y2 - x2*y1)
        ) /
                (
                        (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
                );
        double y = (
                (y3 - y4)*(x1*y2 - x2*y1) - (y1 - y2)*(x3*y4 - x4*y3)
        ) /
                (
                        (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
                );

        //round all the values
        x = Math.round(x); y = Math.round(y);
        x1 = Math.round(x1); x2 = Math.round(x2); x3 = Math.round(x3); x4 = Math.round(x4);
        y1 = Math.round(y1); y2 = Math.round(y2); y3 = Math.round(y3); y4 = Math.round(y4);

        //check that the final point is actualy in a position that intersects both lines
        if((((x < x3 && x < x4) || (x > x3 && x > x4))) || (((x < x1 && x < x2) || (x > x1 && x > x2)))
                || ((y < y3 && y < y4) || (y > y3 && y > y4)) || ((y < y1 && y < y2) || (y > y1 && y > y2)))
        {
            return new Point2D(x2, y2);
        }
        else
        {
            return new Point2D(x, y);
        }
    }

    public Point2D getStartPosition()
    {
        return startPosition.getPoint2D();
    }

    public Point2D getEndPosition()
    {
        return endPosition.getPoint2D();
    }

    /**
     * The draw method of the Laser.
     * The laser draws itself and calls the draw method of its
     * laser child if it has one
     */
    @Override
    public void draw() {
        widget.draw();
        if(childLaser != null)
        {
            childLaser.draw();
        }
    }
}
