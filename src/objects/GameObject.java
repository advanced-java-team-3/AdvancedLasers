package objects;

import core.Game;
import data.ALObject;
import data.LaserColor;
import data.Point3DSerializable;
import graphics.Widget;
import javafx.geometry.Point3D;
import javafx.scene.shape.Polygon;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.io.*;

/**
 * The base class for classes responsible for the behaviour of objects in the
 * game.
 * Created by shaun on 16/09/2016.
 */
public class GameObject implements Serializable {

    transient protected Game game;
    protected Widget widget;
    private Point3DSerializable midPosition;
    private double width;
    private double height;
    private double rotationInDegrees;
    private double rotationOffsetInDegrees;
    private double[] xPoints;
    private double[] yPoints;
    private int totalPoints;
    private double[] xBounds;
    private double[] yBounds;
    private double boundsOffset;
    private ALObject objectType;
    private LaserColor color;
    private boolean fixedPosition;

    @SuppressWarnings("unchecked")
    public GameObject(Game game, Widget widget) {
        this.game = game;
        this.widget = widget;
        this.widget.setGameObject(this);
        midPosition = new Point3DSerializable(0, 0, 0);
        totalPoints = 4;
        setRotationOffset(0);
        color = LaserColor.WHITE;
        fixedPosition = false;
    }

    public void setGame(Game g) {
        game = g;
    }

    /**
     * Only used by the laser pointer, which overrides
     * the method so it can call recalculate
     */
    public void update() {
    }

    public void draw() {
        widget.draw();
    }

    /**
     * Takes rectangle paramaters and uses the offset and reverse maths
     * to create final width and height values that reflect users intentions
     */
    public void setAsRectangle(double width, double height) {
        //set the rectangular offset
        setRotationOffset(45);
        totalPoints = 4;
        double angleOffsetInRadians = Math.toRadians(rotationOffsetInDegrees);
        //get the diagonal width and height necessary to get the intended width and height
        double finalWidth = ((width / 2 - midPosition.getPoint3D().getX()) / (Math.sin(angleOffsetInRadians))) * 2;
        double finalHeight = ((height / 2 - midPosition.getPoint3D().getY()) / (Math.cos(angleOffsetInRadians))) * 2;
        setWidth(finalWidth);
        setHeight(finalHeight);
    }

    /**
     * Takes circle paramaters of radius
     * and creates final width and height values
     */
    public void setAsCircle(double radius) {
        totalPoints = 22;
        setWidth(radius * 2);
        setHeight(radius * 2);
    }

    /**
     * Should be called whenever the object has changed position, rotation or size
     * Updates the polygon for both the visual polygon and the bounds
     */
    private void updatePolygons() {
        //update the visual polygons
        updatePolygon(true);
        //if a bounds offset then update the bounds, else set bounds to visual
        if (boundsOffset != 0) {
            updatePolygon(false);
        }
        else {
            xBounds = xPoints;
            yBounds = yPoints;
        }
    }

    /**
     * Updates the x & y positions for either the visual or bounds polygon
     * Based on this: https://en.wikipedia.org/wiki/Regular_polygon
     */
    private void updatePolygon(boolean visual) {
        //get the degrees addition
        double angleInDegreesPerPoint = 360 / totalPoints;

        //convert degrees to radians
        double angleInRadians = Math.toRadians(getRotation());
        double angleOffsetInRadians = Math.toRadians(rotationOffsetInDegrees);
        double angleInRadiansPerPoint = Math.toRadians(angleInDegreesPerPoint);

        double[] tempXPoints = new double[totalPoints];
        double[] tempYPoints = new double[totalPoints];

        // + Math.toRadians(90)

        double objectWidth = getWidth() / 2;
        double objectHeight = getHeight() / 2;

        if (!visual) {
            objectWidth = objectWidth + boundsOffset;
            objectHeight = objectHeight + boundsOffset;
        }

        //for each point
        for (int i = 0; i < totalPoints; i++) {
            double finalAngle = angleOffsetInRadians + (angleInRadiansPerPoint * i);
            double newPointX = midPosition.getPoint3D().getX() + ((objectWidth) * (Math.sin(finalAngle)));
            double newPointY = midPosition.getPoint3D().getY() + ((objectHeight) * (Math.cos(finalAngle)));
            newPointX = Math.round(newPointX);
            newPointY = Math.round(newPointY);

            //rotate on the angleInRadians
            Point2D tempPoint = rotatePoint(newPointX, newPointY, angleInRadians);
            tempXPoints[i] = tempPoint.getX();
            tempYPoints[i] = tempPoint.getY();
        }

        if (visual) {
            xPoints = tempXPoints;
            yPoints = tempYPoints;
        }
        else {
            xBounds = tempXPoints;
            yBounds = tempYPoints;
        }
    }

    private Point2D rotatePoint(double oldXPoint, double oldYPoint, double angle) {
        Point2D[] tempPoints = new Point2D[]{new Point2D.Double(oldXPoint, oldYPoint)};
        AffineTransform.getRotateInstance
                (angle, midPosition.getPoint3D().getX(), midPosition.getPoint3D().getY())
                .transform(new Point2D[]{new Point2D.Double(oldXPoint, oldYPoint)}, 0, tempPoints, 0, 1);
        return tempPoints[0];
    }

    /**
     * Sets the amount of points the generated polygon has
     * Different points will create shapes based on this:
     * https://en.wikipedia.org/wiki/Regular_polygon
     */
    void setRotation(double rotation) {
        this.rotationInDegrees = rotation;
        updatePolygons();
    }

    /**
     * Applies a rotation offset to the object before the polygon points are calculated
     * Should be used only when the object is initialised,
     * used to turn the diamond shape into a rectangle
     * however the width and height will continue to refer to
     * the distance from the centre and each point
     */
    private void setRotationOffset(double rotation) {
        this.rotationOffsetInDegrees = rotation;
        updatePolygons();
    }

    public void setPosition(Point3D position) {
        this.midPosition.setPoint3D(position);
        updatePolygons();
    }

    public void setPosition(double x, double y, double z) {

        setPosition(new Point3D(x, y, z));
        updatePolygons();
    }

    public void setWidth(double w) {
        width = w;
        updatePolygons();
    }

    public void setHeight(double h) {
        height = h;
        updatePolygons();
    }

    void setTotalPoints(int points) {
        totalPoints = points;
    }

    public void setBoundsOffset(double offset) {
        boundsOffset = offset;
        updatePolygons();
    }

    public Game getGame() {
        return game;
    }

    public Point3D getPosition() {
        return midPosition.getPoint3D();
    }

    public double getRotation() {
        return rotationInDegrees;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public Polygon getBounds() {
        Polygon polygon = new Polygon();
        for (int i = 0; i < totalPoints; i++) {
            polygon.getPoints().addAll(xBounds[i], yBounds[i]);
        }
        return polygon;
    }

    public double[] getXPoints() {
        return xPoints;
    }

    public double[] getYPoints() {
        return yPoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public double[] getXBounds() {
        return xBounds;
    }

    public double[] getYBounds() {
        return yBounds;
    }

    public double getBoundsOffset() {
        return boundsOffset;
    }

    public GameObject deepClone(Game newGame) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(this);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);

            GameObject newGameObject = (GameObject) ois.readObject();
            // IMPORTANT! Set game of new object as it's not serialised
            newGameObject.setGame(newGame);
            return newGameObject;
        }
        catch (IOException e) {
            return null;
        }
        catch (ClassNotFoundException e) {
            return null;
        }
    }

    public ALObject getObjectType() {
        return objectType;
    }

    public void setObjectType(ALObject objectType) {
        this.objectType = objectType;
    }

    public LaserColor getColor() {
        return color;
    }

    public void setColor(LaserColor color) {
        this.color = color;
    }

    public void setFixedPosition(boolean f) {
        fixedPosition = f;
    }

    public boolean getFixedPosition() {
        return fixedPosition;
    }
}