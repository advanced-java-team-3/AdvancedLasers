package objects;

import core.Game;
import data.LaserColor;
import graphics.TargetWidget;

import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * A game object that activates when hit by a certain color of light.
 * When all targets in a level are activated, the player wins that level.
 * Created by Daniel on 10/15/2016.
 */
public class Target extends SolidObject {

    private boolean active;
    private ArrayList<Laser> lasersHit;

    public Target(Game game, LaserColor color) {
        super(game, new TargetWidget());
        setColor(color);
        active = false;
        lasersHit = new ArrayList<>();
    }

    /**
     * OnLaserHit override that lets the laser pass through
     * and adds the laser to a list of lasers
     * that have hot the target
     *
     * @param laser   The laser segment that hit this object.
     * @param edgeHit The edge of this object's geometry that was hit.
     */
    @Override
    public void onLaserHit(Laser laser, Line2D.Double edgeHit) {
        if (!lasersHit.contains(laser)) {
            lasersHit.add(laser);
        }

        laser.createChild(this, laser.getRotation(), laser.getColor());
    }

    /**
     * Goes through all lasers that have hit the target in the most
     * recent update step and check if the target is active.
     * Target is active if more than one laser of the correct color is hitting
     * it and no lasers of the incorrect color are hitting it
     */

    public void checkIfActive() {

        //check that the lasers hitting the target are the correct color
        if (lasersHit.size() > 0) {
            active = true;
            for (int i = 0; i < lasersHit.size(); i++) {
                //if any of the lasers are the wrong color then not active
                // and stop checking
                if (lasersHit.get(i).getColor() != getColor()) {
                    active = false;
                    i = lasersHit.size();
                }
            }
        }
        else //if not hit then not active
            active = false;

        if (!active)//inform the world that the target is not active
            game.addATargetNotActive();

        //clear the list of lasers
        lasersHit.clear();
    }

    public boolean isActive() {
        return active;
    }
}
