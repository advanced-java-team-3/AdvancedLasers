package objects;

import core.Game;
import data.LaserColor;
import graphics.FilterWidget;

import java.awt.geom.Line2D;

/**
 * A game object that alters the color of, or blocks incoming laser light.
 * Created by shaun on 6/10/2016.
 */
public class Filter extends SolidObject {

    public Filter(Game game, LaserColor color) {
        super(game, new FilterWidget());
        setColor(color);
    }

    public Filter(Game game, LaserColor color, double width, double height, int sides, double rotation) {
        super(game, new FilterWidget(), width, height, sides, rotation);
        setColor(color);
    }

    /**
     * If filter allows the lasers color through it then creates new laser
     * @param laser The laser segment that hit this object.
     * @param edgeHit The edge of this object's geometry that was hit.
     */
    @Override
    public void onLaserHit(Laser laser, Line2D.Double edgeHit) {
        LaserColor c = getNewColor(laser.getColor());

        if (c != null)
            laser.createChild(this, laser.getRotation(), c);
    }

    /**
     * Code to check what color the laser will be after it hits the filter
     * @param c The color of the laser before it hits the filter
     * @return The color of the laser after it hits the filter, returns null
     * if the light is not allowed through
     */

    private LaserColor getNewColor(LaserColor c) {
        // laser passes through if filter is white
        if (getColor() == LaserColor.WHITE)
            return c;

        // laser passes through if laser matches filter
        if (getColor() == c)
            return c;

        // white laser changes to colour of filter
        if (c == LaserColor.WHITE)
            return getColor();

        // laser changes to the filter's color if the filter is a component
        // primary color
        if (getColor().isComponentOf(c))
            return getColor();

        // laser changes to shared primary color if the filter and laser are
        // secondary
        if (getColor().isSecondary() && c.isSecondary())
            return getColor().getShared(c);

        // laser passes through if it is a component primary color of the filter
        if (c.isComponentOf(getColor()))
            return c;

        // otherwise, the laser is blocked
        return null;
    }
}
