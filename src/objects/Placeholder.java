package objects;

import core.Game;
import graphics.PlaceholderWidget;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.shape.Polygon;

/**
 * Allows a game object to be moved around the world by the player, while
 * retaining its state.
 * Created by shaun on 1/10/2016.
 */
public class Placeholder extends GameObject {
    private GameObject holding;
    private PlaceholderWidget placeholderWidget;
    private boolean colliding = false;

    public boolean isColliding() {
        return colliding;
    }

    /**
     * Creates a placeholder for a game object and adds it to the world.
     * @param game The game.
     * @param go The game object to be replaced with the placeholder.
     * @return The new placeholder containing the game object.
     */
    public static Placeholder add(Game game, GameObject go) {
        Placeholder result = new Placeholder(game, go);
        game.getWorld().remove(go);
        game.getWorld().add(result);
        return result;
    }

    private Placeholder(Game game, GameObject go) {
        super(game, new PlaceholderWidget(go.widget));
        placeholderWidget = (PlaceholderWidget) widget;
        holding = go;
    }

    public void removeHolding() {
        game.getWorld().remove(this);
        holding = null;
    }

    @Override
    public Polygon getBounds() {
        return holding.getBounds();
    }

    @Override
    public void update() {
        updateRotation();
        Point2D pos = game.getInput().mousePosition();
        setPosition(pos.getX(), pos.getY(), 2);
        game.getWorld().snap(this);
        holding.setPosition(getPosition());

        colliding = game.getWorld().getOverlapping(this) != null;
    }

    /**
     * Places the original game object into the game world at the current
     * position if it is not overlapping another object.
     * If the item was placed in edit mode then sets the fixed position
     * boolean in the game object to true
     * @return <code>true</code> if the object was placed, <code>false</code>
     * otherwise.
     */
    public boolean tryPlace() {
        if (colliding)
            return false;

        placeholderWidget.restore();

        Point3D pos = holding.getPosition();
        holding.setPosition(pos.getX(), pos.getY(), 1);

        game.getWorld().add(holding);
        game.getWorld().remove(this);

        //set position to fixed if in editor mode
        if(game.getGameMode() == Game.GameMode.EDITOR) {
            holding.setFixedPosition(true);
        }

        return true;
    }

    private void updateRotation() {
        double delta = Math.round(game.getInput().mouseScrollDelta());
        // reduce to magnitude of 1
        delta = Math.signum(delta);
        // add or subtract 15 from rotation
        double rotation = (holding.getRotation() + 15 * delta) % 360;
        holding.setRotation(rotation);
    }

    public GameObject getHolding() {
        return holding;
    }
}
