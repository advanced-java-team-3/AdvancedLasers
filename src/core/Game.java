package core;

import data.InputCommand;
import data.Options;
import graphics.Renderer;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseButton;
import objects.GameObject;
import objects.Placeholder;
import ui.ErrorListener;
import ui.UIListener;
import ui.UpdateUIEvent;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Coordinates the interaction between outer subsystems and the game.
 * Specifically, responds to user interaction, game ticks and the user
 * interface.
 * Created by shaun on 15/09/2016.
 */
public class Game {

    private Engine engine;
    private World world;

    private boolean debugMode = false;
    private Placeholder moving;

    // Mode
    public enum GameMode {
        MAINGAME, EDITOR
    }

    private GameMode gameMode = GameMode.MAINGAME;
    private int targetsNotActive = 0;

    // UI
    private boolean updateUI = true;
    private final CopyOnWriteArrayList<UIListener> listeners =
            new CopyOnWriteArrayList<>();
    private final CopyOnWriteArrayList<ErrorListener> errorListeners =
            new CopyOnWriteArrayList<>();

    public Game(Engine e) {
        engine = e;
        world = new World(this, Options.GRID_X_SNAP, Options.GRID_Y_SNAP);
    }

    /**
     * Updates the game, done every tick
     * Updates any currently held objects / adds and removed them if necessary
     * Performs actions based on the input commands
     * Clears targets activated in the worlds level
     * Updates the world
     * Then completes the level if all targets active
     */
    public void update() {
        Inventory inv = world.getCurrentLevel().getInventory();

        // If changed, we need to update the currently held object
        if (inv.isSelectedChanged()) {
            Point2D pos = getInput().mousePosition();
            GameObject clicked = world.getContaining(pos);
            if (gameMode == GameMode.MAINGAME)
                clicked = inv.takeSelectedItem(this);
            else
                if (gameMode == GameMode.EDITOR)
                    clicked = inv.editorTakeItem(this);

            if (moving != null)
                moving.removeHolding();
            if (clicked != null)
                moving = Placeholder.add(this, clicked);
        }

        // If mouse pressed, place currently held. If nothing held, try
        // picking up a piece
        if (getInput().mousePressed()) {
            if (moving != null) {
                if (moving.tryPlace()) {
                    moving = null;
                    if (inv.getSelectedSlot() != null
                            && gameMode == GameMode.MAINGAME)
                        inv.getSelectedSlot().inGameReduceCount();
                    inv.selectSlot(null);
                }
            }
            else {
                Point2D pos = getInput().mousePosition();
                GameObject clicked = world.getContaining(pos);
                if (clicked != null) {
                    //only pick up item if in editor mode OR if not in edit
                    // mode and fixed position is false
                    if (gameMode == GameMode.EDITOR ||
                            (gameMode == GameMode.MAINGAME &&
                                    !clicked.getFixedPosition())) {
                        // If ctrl-click, copy selected item
                        if (getInput().isCtrlDown())
                            moving = Placeholder.add(this,
                                    clicked.deepClone(this));
                        else
                            moving = Placeholder.add(this, clicked);
                        moving.setFixedPosition(false);
                    }
                }
            }
            updateUI = true;
        }

        // If secondary pressed, remove if holding something
        if (getInput().mousePressed(MouseButton.SECONDARY)) {
            deselectItem(inv);
            updateUI = true;
        }

        if (getInput().commandRequested(InputCommand.CLEAR)) {
            world.clearLevel(this);
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.MAINGAME)) {
            gameMode = GameMode.MAINGAME;
            deselectItem(inv);
            world.setAndLoadLevel(0);
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.EDITOR)) {
            gameMode = GameMode.EDITOR;
            deselectItem(inv);
            world.loadLevel();
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.DEBUG)) {
            debugMode = !debugMode;
        }
        if (getInput().commandRequested(InputCommand.SAVE_CURRENT)) {
            deselectItem(inv);
            world.saveLevel();
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.LOAD_NEXT_LEVEL)) {
            if (world.getPlayersSave().hasLevelbeenBeaten(world.getCurrentLevelName()) || gameMode == GameMode.EDITOR) {
                world.loadNextLevel();
                updateUI = true;
            }
        }
        if (getInput().commandRequested(InputCommand.LOAD_PREVIOUS_LEVEL)) {
            world.loadPreviousLevel();
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.INCREASE_INVENTORY)) {
            if (inv.getSelectedSlot() != null) {
                inv.getSelectedSlot().editorIncreaseAmount();
            }
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.DECREASE_INVENTORY)) {
            if (inv.getSelectedSlot() != null) {
                inv.getSelectedSlot().editorDecreaseAmount();
            }
            updateUI = true;
        }
        if (getInput().commandRequested(InputCommand.UPDATE_UI)) {
            updateUI = true;
        }

        //reset targets not active to 0
        targetsNotActive = 0;

        //update all objects
        world.update();

        //if targets not active is 0 AND not in edit mode then beat the level
        if (targetsNotActive == 0 && getGameMode().equals(GameMode.MAINGAME))
            getWorld().setCurrentLevelBeaten();
    }

    public void draw() {
        world.draw();
        if (updateUI) {
            this.fireChangeEvent();
            updateUI = false;
        }
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public boolean getDebugMode() {
        return debugMode;
    }

    public Input getInput() {
        return engine.getInput();
    }

    public Renderer getRenderer() {
        return engine.getRenderer();
    }

    public Screen getScreen() {
        return engine.getScreen();
    }

    public ArrayList<GameObject> getGameObjects() {
        return world.getGameObjects();
    }

    public World getWorld() {
        return world;
    }

    int getTargetsNotActive() {
        return targetsNotActive;
    }

    public void addMyChangeListener(UIListener l) {
        this.listeners.add(l);
    }

    public void addErrorListener(ErrorListener l) {
        this.errorListeners.add(l);
    }

    /**
     * Notifies all listeners that an error occurred.
     *
     * @param message The message to send.
     */
    public void onError(String message) {
        Platform.runLater(() -> {
            errorListeners.forEach(l -> l.onError(message, null));
        });
    }

    /**
     * Notifies all listeners that an error occurred.
     *
     * @param message The message to send.
     * @param e       The exception that occurred.
     */
    public void onError(String message, Exception e) {
        errorListeners.forEach(l -> l.onError(message, e));
    }

    /**
     * Increments target not active by 1
     * Targets will call this method if they are not activated in an update
     */
    public void addATargetNotActive() {
        targetsNotActive = targetsNotActive + 1;
    }

    /**
     * Event firing method.  Called internally by other class methods.
     */
    private void fireChangeEvent() {
        UpdateUIEvent evt = new UpdateUIEvent(this);

        for (UIListener l : listeners) {
            l.UIListener(evt);
        }
    }

    /**
     * Removed theinventory item being held by the player if there is one
     *
     * @param inv The worlds current level inventory
     */
    private void deselectItem(Inventory inv) {
        if (moving != null) {
            if (gameMode == GameMode.MAINGAME) {
                InventorySlot slotToReturn =
                        inv.getSlot(moving.getHolding().getObjectType());
                if (slotToReturn != null) {
                    slotToReturn.inGameReturn();
                }
                inv.setSelectedChanged(true);
            }
            moving.removeHolding();
            moving = null;
        }
        inv.selectSlot(null);

        updateUI = true;
    }

}
