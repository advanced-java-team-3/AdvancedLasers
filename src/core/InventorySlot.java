package core;

import data.ALObject;
import objects.GameObject;

import java.io.Serializable;

/**
 * Created by Timothy on 10/10/2016.
 */
public class InventorySlot implements Serializable {
    private ALObject alObjectType;
    private GameObject gameObjectToClone;

    private int maxAmount;
    private int currentAmount;

    private boolean selected;

    InventorySlot(ALObject alObjectType,
                  GameObject gameObjectToClone,
                  int amount) {

        this.alObjectType = alObjectType;
        this.gameObjectToClone = gameObjectToClone;
        this.maxAmount = amount;
        this.currentAmount = amount;
        this.selected = false;
    }

    GameObject inGameTakeFrom(Game game) {
        return gameObjectToClone.deepClone(game);
    }

    void inGameReduceCount() {
        --currentAmount;
    }

    void inGameReturn() {
        ++currentAmount;
    }

    GameObject editorTakeFrom(Game game) {
        return gameObjectToClone.deepClone(game);
    }

    void editorIncreaseAmount() {
        this.maxAmount += 1;
    }

    void editorDecreaseAmount() {
        if (this.maxAmount > 0)
            this.maxAmount -= 1;
    }

    public boolean isSelected() {
        return selected;
    }

    public int getCurrentAmount() {
        return currentAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ALObject getAlObjectType() {
        return alObjectType;
    }
}
