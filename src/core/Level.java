package core;

import objects.GameObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Contains the game objects in the game world and player's inventory.
 * Created by Daniel on 10/7/2016.
 */
public class Level implements Serializable {

    private ArrayList<GameObject> gameObjects;
    private Inventory inventory;

    Level(Game game) {
        gameObjects = new ArrayList<>();
        inventory = new Inventory(game);
    }

    /**
     * Sets the game to the game parameter for every object and the inventory
     * in this level. Necessary due to loading from serialization having links
     * to an out of date game reference.
     *
     * @param game The new game to be referenced by all objects
     */
    void setGameForAllLevelData(Game game) {
        for (GameObject go : gameObjects) {
            go.setGame(game);
        }
        updateInventory(game);
    }

    private void removeAllGameObjects() {
        gameObjects.clear();
    }

    /**
     * Clear level removes all game objects in the ARrayList and
     * creates a new inventory
     *
     * @param game The game reference required for updating the inventory
     */
    void clearLevel(Game game) {
        removeAllGameObjects();
        inventory = new Inventory(game);
    }

    /**
     * Creates an inventory if it is null or updates the inventory
     *
     * @param game The game reference required for updating the inventory
     */
    private void updateInventory(Game game) {
        if (inventory == null) {
            inventory = new Inventory(game);
        }
        else {
            inventory.updateInventory(game);
        }
    }

    public Inventory getInventory() {
        return inventory;
    }

    ArrayList<GameObject> getGameObjects() {
        return gameObjects;
    }

    void addGameObject(GameObject go) {
        gameObjects.add(go);
    }

    void removeGameObject(GameObject go) {
        gameObjects.remove(go);
    }
}
