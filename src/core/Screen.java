package core;

/**
 * Maintain's information about the game world's viewport
 * Created by shaun on 15/09/2016.
 */
public class Screen {

    double width;
    double height;

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
}
