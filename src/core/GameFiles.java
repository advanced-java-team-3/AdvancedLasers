package core;

import java.io.*;

/**
 * Serializes and deserializes objects to and from files.
 * Created by shaun on 24/10/2016.
 */
class GameFiles {
    /**
     * Saves any serializable (player save or level)
     * @param path The location to save the serializable object
     * @param object The object to be saved
     */
    static void save(File path, Serializable object)
            throws IOException {

        path.getParentFile().mkdirs();

        // try-with-resources statement, the streams will be closed regardless
        // of whether or not an exception occurs
        try (
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut)
        ) {
            out.writeObject(object);
        }
    }

    /**
     * Loads a serializable object from the specified file.
     * @param path The file to deserialize.
     * @return The deserialized object.
     * @throws IOException if an I/O error occurs while reading the file.
     * @throws ClassNotFoundException if the serialized object refers to an
     * unknown class.
     */
    static Serializable load(File path)
            throws IOException, ClassNotFoundException {

        try (
            FileInputStream inStream = new FileInputStream(path);
            ObjectInputStream stream = new ObjectInputStream(inStream)
        ) {
            return (Serializable) stream.readObject();
        }
    }
}
