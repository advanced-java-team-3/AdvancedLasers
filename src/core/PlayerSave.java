package core;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Maintains information about the player's progress through the game.
 * Created by Daniel on 10/15/2016.
 */
class PlayerSave implements Serializable {
    private ArrayList<String> levelsBeaten;

    PlayerSave() {
        levelsBeaten = new ArrayList<>();
    }

    /**
     * Adds the name of a level beaten to the players level beaten list
     *
     * @param levelName Name of the level that has been beaten
     */
    void addLevelBeaten(String levelName) {
        levelsBeaten.add(levelName);
    }

    /**
     * Queries the levelsBeaten list to determine if a level has been beaten
     *
     * @param levelName Name of the level to query
     * @return Returns true if the level named has been beaten
     */
    boolean hasLevelbeenBeaten(String levelName) {
        return levelsBeaten.contains(levelName);
    }
}
