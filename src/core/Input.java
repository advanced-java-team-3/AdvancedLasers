package core;

import data.InputCommand;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.HashSet;

/**
 * Maintains the current state of key and button presses and mouse position.
 * Created by shaun on 15/09/2016.
 */
public class Input {
    /**
     * Keys that are currently down.
     */
    private HashSet<KeyCode> keysDown = new HashSet<>();
    /**
     * Keys that have become down since the last tick.
     */
    private HashSet<KeyCode> keysPressed = new HashSet<>();
    /**
     * Keys that have become up since the last tick.
     */
    private HashSet<KeyCode> keysReleased = new HashSet<>();
    /**
     * Mouse buttons that are currently down.
     */
    private HashSet<MouseButton> mouseButtonsDown = new HashSet<>();
    /**
     * Mouse buttons that have become down since the last tick.
     */
    private HashSet<MouseButton> mouseButtonsPressed = new HashSet<>();
    /**
     * Mouse buttons that have become up since the last tick.
     */
    private HashSet<MouseButton> mouseButtonsReleased = new HashSet<>();

    private boolean ctrlDown = false;

    private HashSet<InputCommand> inputCommandsToIssue = new HashSet<>();

    private Point2D mousePosition = new Point2D(0, 0);

    private double scrollDelta;

    public Input(Node target) {

        target.getScene().setOnKeyPressed(e -> {
            keysDown.add(e.getCode());
            keysPressed.add(e.getCode());
            keysReleased.remove(e.getCode());

            ctrlDown = e.isControlDown();
        });

        target.getScene().setOnKeyReleased(e -> {
            keysDown.remove(e.getCode());
            keysPressed.remove(e.getCode());
            keysReleased.add(e.getCode());

            ctrlDown = e.isControlDown();
        });

        target.setOnMousePressed(e -> {
            mouseButtonsDown.add(e.getButton());
            mouseButtonsPressed.add(e.getButton());
            mouseButtonsReleased.remove(e.getButton());

            ctrlDown = e.isControlDown();
        });

        target.setOnMouseReleased(e -> {
            mouseButtonsDown.remove(e.getButton());
            mouseButtonsPressed.remove(e.getButton());
            mouseButtonsReleased.add(e.getButton());

            ctrlDown = e.isControlDown();
        });

        target.setOnMouseMoved(e ->
                mousePosition = new Point2D(e.getX(), e.getY()));

        target.setOnMouseDragged(e ->
                mousePosition = new Point2D(e.getX(), e.getY()));

        target.setOnScroll(e ->
                scrollDelta = e.getDeltaY());
    }

    /**
     * Whether the specified key is down and was up in the previous tick.
     * @param key the key to check.
     * @return {@code true} if the key is down and was up in the previous tick.
     */
    public boolean keyPressed(KeyCode key) {
        return keysPressed.contains(key);
    }

    /**
     * Whether the specified key is down.
     * @param key the key to check.
     * @return {@code true} if the key is down.
     */
    public boolean keyDown(KeyCode key) {
        return keysDown.contains(key);
    }

    /**
     * Whether the key is up and was down in the previous tick.
     * @param key the key to check.
     * @return {@code true} if the key is up and was down in the previous tick.
     */
    public boolean keyReleased(KeyCode key) {
        return keysReleased.contains(key);
    }

    /**
     * Whether the specified button is down and was up in the previous tick.
     * @param button the button to check.
     * @return {@code true} if the button is down and was up in the previous
     * tick.
     */
    boolean mousePressed(MouseButton button) {
        return mouseButtonsPressed.contains(button);
    }

    /**
     * Whether the primary mouse button is down and was up in the previous tick.
     * @return {@code true} if the button is down and was up in the previous
     * tick.
     */
    public boolean mousePressed() {
        return mousePressed(MouseButton.PRIMARY);
    }

    /**
     * Whether the specified button is down.
     * @param button the button to check.
     * @return {@code true} if the button is down.
     */
    private boolean mouseDown(MouseButton button) {
        return mouseButtonsDown.contains(button);
    }

    /**
     * Whether the primary mouse button is down.
     * @return {@code true} if the button is down.
     */
    public boolean mouseDown() {
        return mouseDown(MouseButton.PRIMARY);
    }

    /**
     * Whether the button is up and was down in the previous tick.
     * @param button the button to check.
     * @return {@code true} if the button is up and was down in the previous
     * tick.
     */
    private boolean mouseReleased(MouseButton button) {
        return mouseButtonsReleased.contains(button);
    }

    /**
     * Whether the primary mouse button is up and was down in the previous tick.
     * @return {@code true} if the button is up and was down in the previous
     * tick.
     */
    public boolean mouseReleased() {
        return mouseReleased(MouseButton.PRIMARY);
    }

    /**
     * The current mouse position.
     * @return the mouse position.
     */
    public Point2D mousePosition() {
        return mousePosition;
    }

    /**
     * The direction and amount the scroll wheel has moved since the last tick.
     * @return the magnitude of the scroll as positive if the scroll is down, or
     * negative if the scroll is up.
     */
    public double mouseScrollDelta() {
        return scrollDelta;
    }

    /**
     * Call this at the very end of each tick to clear the pressed and released
     * values for the next frame.
     */
    public void afterUpdate() {
        keysPressed.clear();
        keysReleased.clear();
        mouseButtonsPressed.clear();
        mouseButtonsReleased.clear();
        inputCommandsToIssue.clear();
        scrollDelta = 0;
    }

    public void requestCommand(InputCommand inputCommand) {
        inputCommandsToIssue.add(inputCommand);
    }

    boolean commandRequested(InputCommand inputCommand) {
        return inputCommandsToIssue.contains(inputCommand);
    }

    boolean isCtrlDown() {
        return ctrlDown;
    }
}
