package core;

import data.Options;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import objects.GameObject;
import objects.Target;
import ui.Grid;

import java.io.*;
import java.util.ArrayList;

/**
 * Manages the objects that have been placed in the game world.
 * Created by shaun on 23/09/2016.
 */
public class World {

    private Game game;

    private Level currentLevel;
    private String currentLevelName;
    private ArrayList<String> levelFileNames;
    private int currentLevelPosition;
    private String saveOverLevelName;

    private double xIncrement;
    private double yIncrement;
    private Grid grid;

    private PlayerSave save;
    private ArrayList<String> savedGameNames;

    /**
     * Creates a new game world that allows objects to snap to the x and y axes
     * in the specified increments.
     *
     * @param game       The game.
     * @param xIncrement The amount of pixels on the x-axis that objects should
     *                   snap to.
     * @param yIncrement The amount of pixels on the y-axis that objects should
     *                   snap to.
     */
    public World(Game game, double xIncrement, double yIncrement) {
        this.game = game;
        this.xIncrement = xIncrement;
        this.yIncrement = yIncrement;
        this.grid = new Grid(this);
        currentLevel = new Level(game);
        currentLevelPosition = 0;
        currentLevelName = "";
        saveOverLevelName = currentLevelName;
        loadLevel();
        loadPlayersSave();
    }

    public Game getGame() {
        return game;
    }

    /**
     * Gets the amounts of pixels on the x and y axes that objects should snap
     * to.
     * @return a point representing the snap increments for the x and y axes.
     */
    public Point2D getIncrements() {
        return new Point2D(xIncrement, yIncrement);
    }

    public ArrayList<GameObject> getGameObjects() {
        return currentLevel.getGameObjects();
    }

    /**
     * Gets a game object if it contains the position pos
     *
     * @param pos The position to check if it is within a game object
     * @return The game object if it contains a point,
     * null if no object contains a point
     */
    GameObject getContaining(Point2D pos) {
        ArrayList<GameObject> gameObjects = getGameObjects();
        for (GameObject go : gameObjects) {
            if (go.getBounds().contains(pos))
                return go;
        }

        return null;
    }

    /**
     * Returns other gameObject if it overlaps with any game object
     *
     * @param other The gameObject to check if it overlaps
     * @return One of the game objects that other overlaps with
     * null if other overlaps with nothing else
     */
    public GameObject getOverlapping(GameObject other) {
        ArrayList<GameObject> gameObjects = getGameObjects();
        Bounds otherBounds = other.getBounds().getLayoutBounds();

        for (GameObject go : gameObjects) {
            if (go == other)
                continue;

            if (go.getBounds().intersects(otherBounds) &&
                    other.getBounds().intersects(
                            go.getBounds().getLayoutBounds())) {
                return go;
            }
        }

        return null;
    }

    /**
     * Adds a new game object to the world.
     * The object will be placed at the nearest snap point to its current
     * position.
     *
     * @param go The game object to add.
     */
    public void add(GameObject go) {
        snap(go);
        currentLevel.addGameObject(go);
    }

    /**
     * Snaps the x & y postions of a gameObject parameter
     *
     * @param go The object to snap its x & y positions
     */
    public void snap(GameObject go) {
        Point3D pos = go.getPosition();
        long xMultiple = Math.round(pos.getX() / xIncrement);
        long yMultiple = Math.round(pos.getY() / yIncrement);
        go.setPosition(
                xIncrement * xMultiple,
                yIncrement * yMultiple,
                pos.getZ());
    }

    public void remove(GameObject go) {
        currentLevel.removeGameObject(go);
    }

    public void clearLevel(Game game) {
        currentLevel.clearLevel(game);
    }

    /**
     * Updates the World.
     * Goes through each object in the current level and updates it.
     * If the object is a target then adds it to a list of targets.
     * After all objects have been updated then checks
     * the active state of each target.
     */
    public void update() {
        ArrayList<GameObject> gameObjects = getGameObjects();
        ArrayList<Target> targets = new ArrayList<>();
        for (GameObject go : gameObjects) {
            go.update();
            if (go instanceof Target)
                targets.add((Target) go);
        }
        for (Target t : targets) {
            t.checkIfActive();
        }
    }

    /**
     * Draws the world.
     * Draws the grid of the world, gets every game object in the currentlevel
     * then draws every game object
     */
    public void draw() {
        grid.draw();
        ArrayList<GameObject> gameObjects = getGameObjects();
        gameObjects.forEach(GameObject::draw);
    }

    /**
     * Loads the level idenfified by the currentLevelName
     */
    public void loadLevel() {
        changeCurrentLevelPosition();
        //if there is at least 1 save file found
        if (levelFileNames.size() > 0) {
            loadLevelNamed(currentLevelName);
        }
    }

    /**
     * Sets the name of the level at the currentLevelPosition
     */
    private void setNameOfCurrentPosition() {
        if (levelFileNames.size() > 0) {
            currentLevelName = levelFileNames.get(currentLevelPosition);
        }
        saveOverLevelName = currentLevelName;
    }

    /**
     * Sets the position of the level based on the current name
     */
    private void setPositionOfCurrentLevel() {
        currentLevelPosition = 0;
        for (int i = 0; i < levelFileNames.size(); i++) {
            if (levelFileNames.get(i).equals(currentLevelName)) {
                currentLevelPosition = i;
            }
        }
    }

    /**
     * Saves the level as the saveOverLevelName variable
     */
    public void saveLevel() {
        //if there is a current level and there are objects in the level
        if (currentLevel != null && currentLevel.getGameObjects().size() > 0) {
            //save over level with save over level name
            saveCurrentLevelAs(saveOverLevelName);
            getAllLevelFiles();
            setPositionOfCurrentLevel();
        }
        else {
            game.onError("There is nothing here to save.");
        }
    }

    /**
     * Goes to the next level in the list then loads it
     */
    void loadNextLevel() {
        currentLevelPosition = currentLevelPosition + 1;
        loadLevel();
    }

    /**
     * Goes to the previous level in the list then loads it
     */
    void loadPreviousLevel() {
        currentLevelPosition = currentLevelPosition - 1;
        loadLevel();
    }

    /**
     * Changes the current level position
     * Loads all files, wraps position if it is greater than
     * the files or less than 1 then sets the name of the current level
     */
    private void changeCurrentLevelPosition() {
        getAllLevelFiles();
        preventWrapLevelPosition();
        setNameOfCurrentPosition();
    }

    /**
     * Prevent wrap level position
     * If level position is greater than the files list size then make it the final level in the list
     * If level position is less than 0 then make it 0
     */
    private void preventWrapLevelPosition() {
        if (currentLevelPosition >= levelFileNames.size()) {
            currentLevelPosition = levelFileNames.size() - 1;
        }
        else {
            if (currentLevelPosition < 0)
                currentLevelPosition = 0;
        }
    }

    public Level getCurrentLevel() {
        return currentLevel;
    }

    public String getCurrentLevelName() {
        return currentLevelName;
    }

    public String getSaveOverLevelName() {
        return saveOverLevelName;
    }

    /**
     * Gets a complete string of all levels in edit mode
     * Includes the current level and the level to save over
     *
     * @return A complete level string for the UI
     */
    public String getEditorLevelNames() {
        String result = "All Levels:\n";
        for (String s : levelFileNames) {
            result = result + s;
            if (s.equals(currentLevelName)) {
                result = result + "(C)";
            }
            if (s.equals(saveOverLevelName)) {
                result = result + "(S)";
            }
            result = result + "\n";
        }
        return result;
    }

    /**
     * Gets a complete string of all levels in the main game mode
     * Includes the current level and complete levels
     * Uses ??? for unknown levels
     *
     * @return A complete level string for the UI
     */
    public String getLevelInformation() {
        String result = "Targets to Hit: " + game.getTargetsNotActive() + "\n";
        result = result + "All Levels:\n";
        for (String s : levelFileNames) {
            if (getPlayersSave().hasLevelbeenBeaten(s) ||
                    s.equals(currentLevelName)) {

                result = result + "    " + s;

                if (s.equals(currentLevelName)) {
                    result = result + " (CURRENT)";
                }
                else
                    if (getPlayersSave().hasLevelbeenBeaten(s)) {
                        result = result + " (DONE)";
                    }
            }
            else {
                result = result + "    ???";
            }
            result = result + "\n";
        }
        return result;
    }

    /**
     * Saves the players save
     *
     * @param saveName The name of the players save
     */
    private void savePlayersSaveAs(String saveName) {
        try {
            GameFiles.save(new File(Options.SAVE_FILES, saveName), save);
        }
        catch (IOException e) {
            game.onError("Something went wrong while writing your save file " +
                    "to disk.", e);
        }
    }

    /**
     * Saves the current level
     *
     * @param saveName The name of the current level to be saved
     */
    private void saveCurrentLevelAs(String saveName) {
        try {
            GameFiles.save(
                    new File(Options.LEVEL_FILES, saveName), currentLevel);
        }
        catch (IOException e) {
            game.onError("Something went wrong while writing the level to disk.");
        }
    }

    /**
     * Loads a level based on its name
     *
     * @param fileName The name of the level to load
     */
    private void loadLevelNamed(String fileName) {
        Level result = null;

        Serializable loadedObject = loadSerializable(
                Options.LEVEL_FILES, fileName);

        // error has already been logged
        if (loadedObject == null)
            return;

        if (loadedObject instanceof Level) {
            result = (Level) loadedObject;
        }
        else {
            game.onError("Object loaded from '" + fileName +
                    "' was not a level. Perhaps it was created in a " +
                    "different version of Advanced Lasers.");
        }

        if (result != null) {
            currentLevel = result;
            currentLevel.setGameForAllLevelData(game);
        }
    }

    /**
     * Loads a players save.
     * At the moment only the first player save in the players save file is
     * loaded.
     */
    private void loadPlayersSave() {
        getAllSaveFiles();
        if (savedGameNames.size() > 0) {
            Serializable loadedObject = loadSerializable(
                    Options.SAVE_FILES, savedGameNames.get(0));

            if (loadedObject != null) {
                if (loadedObject instanceof PlayerSave) {
                    save = (PlayerSave) loadedObject;
                }
                else {
                    game.onError(
                            "Object loaded from '" + savedGameNames.get(0) +
                            "' was not a player save. Perhaps it was created " +
                            "in a different version of Advanced Lasers.");
                }
            }
        }

        //if save is still null then create a new one
        if (save == null) {
            save = new PlayerSave();
        }
    }

    /**
     * Loads any serializable (player save or level)
     *
     * @param fileDirectory The location of the file to load
     * @param fileName      The name of the file to load
     * @return The serializable object loaded from the file
     */
    private Serializable loadSerializable(String fileDirectory,
                                          String fileName) {

        File file = new File(fileDirectory, fileName);

        try {
            return GameFiles.load(file);
        }
        catch (IOException i) {
            game.onError("The file '" + file.getAbsolutePath() +
                    "' could not be loaded.");
            return null;
        }
        catch (ClassNotFoundException c) {
            game.onError("Something went wrong while reading the contents of " +
                    file.getAbsolutePath() + ". Perhaps this file is for a " +
                    "different version of Advanced Lasers.");
            return null;
        }
    }

    /**
     * Get a list of all saved files in the save directory
     */
    private void getAllSaveFiles() {
        savedGameNames = new ArrayList<>();
        savedGameNames.addAll(getFileNames(Options.SAVE_FILES));
    }

    /**
     * Get a list of all level files in the level directory
     */
    private void getAllLevelFiles() {
        levelFileNames = new ArrayList<>();
        levelFileNames.addAll(getFileNames(Options.LEVEL_FILES));
    }

    /**
     * Get all file names in any directory (level or save)
     *
     * @param fileLocation The location to get all file names from
     * @return AN arrayList of all file names in that directory
     */
    private ArrayList<String> getFileNames(String fileLocation) {
        ArrayList<String> tempStringList = new ArrayList<>();
        File folder = new File(fileLocation);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile() &&
                        !file.isHidden() &&
                        !file.getName().startsWith(".")) {

                    tempStringList.add(file.getName());
                }
            }
        }
        return tempStringList;
    }

    /**
     * Sets the current level as having bean beaten and adds this
     * information to the players save
     */
    void setCurrentLevelBeaten() {
        getPlayersSave().addLevelBeaten(currentLevelName);
        //and save the players progress
        savePlayersSaveAs("PlayersSave");
    }

    public PlayerSave getPlayersSave() {
        return save;
    }

    /**
     * Sets level name
     *
     * @param newName the new level name
     */
    public void setLevelName(String newName) {
        currentLevelName = newName;
        saveOverLevelName = currentLevelName;
    }

    /**
     * Sets current level to params level and loads the level
     *
     * @param levelNumber the level to set and load
     */
    public void setAndLoadLevel(int levelNumber) {
        currentLevelPosition = levelNumber;
        loadLevel();
    }
}
