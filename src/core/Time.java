package core;

/**
 * Contains information about timing.
 * Created by shaun on 15/09/2016.
 */
class Time {
    /**
     * The system time at the start of the game.
     */
    private double start;

    /**
     * The system time at the start of the game.
     * @return the system time in seconds.
     */
    public double start() {
        return start;
    }

    /**
     * The system time at the start of this tick.
     */
    private double current;

    /**
     * The system time at the start of this tick.
     * @return the system time in seconds.
     */
    public double current() {
        return current;
    }

    /**
     * Time between this tick and the last.
     */
    private double delta;

    /**
     * Time between this tick and the last.
     * @return the difference in time in seconds.
     */
    public double delta() {
        return delta;
    }

    /**
     * Time elapsed since the start of the game.
     * @return the time between the first ever tick and the current tick.
     */
    public double elapsed() {
        return current - start;
    }

    /**
     * Call this as early as possible each tick to update the time.
     */
    void update(long currentMicroseconds) {
        double currentSecs = currentMicroseconds / 1.0e9;

        if (start == 0) {
            start = currentSecs;
            current = currentSecs;
            return;
        }

        delta = currentSecs - current;
        current = currentSecs;
    }
}
