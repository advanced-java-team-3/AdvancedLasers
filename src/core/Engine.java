package core;

import graphics.Renderer;
import javafx.scene.Node;
import javafx.scene.canvas.GraphicsContext;

/**
 * Coordinates game subsystems.
 * Created by shaun on 15/09/2016.
 */
public class Engine {
    private Node node;
    private Renderer renderer;

    private Game game = new Game(this);
    private Time time = new Time();
    private Input input;
    private Screen screen = new Screen();

    /**
     * Creates a new engine instance.
     * @param node The JavaFX control for which input is handled and which
     *             screen positions are relative to.
     * @param graphics The graphics context used for drawing game objects.
     */
    public Engine(Node node, GraphicsContext graphics) {
        this.node = node;
        this.renderer = new Renderer(graphics);
        this.input = new Input(node);
    }

    /**
     * Every tick the game updates,
     * is drawn to the screen and
     * clears keys that have been inputted
     * @param currentTime long values of the current time
     */
    public void tick(long currentTime) {
        update(currentTime);
        draw();
        input.afterUpdate();
    }

    /**
     * Called by the tick function
     * Updates the current time and the game
     * @param currentTime long values of the current time
     */
    private void update(long currentTime) {
        time.update(currentTime);
        screen.width = node.getBoundsInParent().getWidth();
        screen.height = node.getBoundsInParent().getHeight();
        game.update();
    }

    /**
     * Called once evert tick
     * Clears the screen then calls draw in the game and the renderer
     */
    private void draw() {
        renderer.getGraphics()
                .clearRect(0, 0, screen.getWidth(), screen.getHeight());

        renderer.getGraphics().save();
        game.draw();
        renderer.draw();
        renderer.getGraphics().restore();
    }

    public Game getGame() {
        return game;
    }

    Time getTime() { return time; }

    Input getInput() { return input; }

    Renderer getRenderer() {
        return renderer;
    }

    Screen getScreen() {
        return screen;
    }
}
