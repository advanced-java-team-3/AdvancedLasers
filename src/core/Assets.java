package core;

import javafx.scene.image.Image;

import java.util.HashMap;

/**
 * Loads and caches assets for quick and easy access.
 * Created by shaun on 16/09/2016.
 */
public class Assets {
    private static HashMap<String, Image> images = new HashMap<>();

    static {
        loadImage("laser_pointer_red");
        loadImage("laser_pointer_green");
        loadImage("laser_pointer_blue");
        loadImage("laser_pointer_cyan");
        loadImage("laser_pointer_magenta");
        loadImage("laser_pointer_yellow");
        loadImage("laser_pointer_white");
    }

    /**
     * Loads an image into the images hash map
     * @param name The name of the image to be loaded
     */
    private static void loadImage(String name) {
        try {
            images.put(name,
                    new Image("/" + name + ".png", false));
        }
        catch (IllegalArgumentException e) {
            // nothing to do here.
            // Simply return null when the player asks for it.
        }
    }

    /**
     * Returns an image from the images hash map
     * @param name The name of the image to be retrieved
     */
    public static Image getImage(String name) {
        return images.get(name);
    }
}
