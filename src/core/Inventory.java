package core;

import data.ALObject;
import objects.GameObject;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Timothy on 10/10/2016.
 */
public class Inventory implements Serializable {
    private LinkedHashMap<ALObject, InventorySlot> invMap
            = new LinkedHashMap<ALObject, InventorySlot>();
    private InventorySlot selectedSlot;
    private boolean selectedChanged;

    Inventory(Game game) {
        createInventory(game);
    }

    /**
     * Adds all necessary slots on startup,
     * giving the game. necessary to create objects
     *
     * @param alObjectType The object type to be added as a slot
     * @param game         A reference to the game to be givem to the object
     *                     created
     */
    private void addSlot(ALObject alObjectType, Game game, int amount) {
        InventorySlot newSlot = new InventorySlot(alObjectType,
                ALObject.create(alObjectType, game), amount);
        invMap.put(alObjectType, newSlot);
    }

    /**
     * Updates a slot with the current game
     * Necessary since inventory slots saved in an older
     * save game will now point to an incorrect game object
     *
     * @param alObjectType The object type to be updated as a new slot
     * @param game         A reference to the current game to be givem to the
     *                     object
     */
    private void updateSlot(ALObject alObjectType, Game game) {
        if (invMap.containsKey(alObjectType)) {
            //get the old ammount
            int oldMaxAmount = invMap.get(alObjectType).getMaxAmount();
            //remove the old slot
            invMap.remove(alObjectType);
            //create and add the new inventory slot
            InventorySlot newSlot = new InventorySlot(alObjectType,
                    ALObject.create(alObjectType, game), oldMaxAmount);
            invMap.put(alObjectType, newSlot);

        }
    }

    /**
     * Gets selected item from a selected inventory slot in the main game mode
     *
     * @param game Takes the game reference to be given to any cloned object
     * @return Returns the object based on the selected slot
     */
    GameObject takeSelectedItem(Game game) {
        if (selectedSlot == null)
            return null;
        else {
            if (selectedSlot.getCurrentAmount() > 0) {
                return selectedSlot.inGameTakeFrom(game);
            }
        }
        return null;
    }

    /**
     * Same as takeSelectedItem but always gives an item regardless of the count
     * Used during edit mode to provide infinite objects to the player
     *
     * @param game Takes the game reference to be given to any cloned object
     * @return Returns the object based on the selected slot
     */
    GameObject editorTakeItem(Game game) {
        if (selectedSlot == null) {
            return null;
        }
        else {
            return selectedSlot.editorTakeFrom(game);
        }
    }

    /**
     * Called on startup, creates the inventory LinkedHashMap for this class
     * and adds a slot for every single game object
     *
     * @param game A reference to the game for the creation of all object slots
     */
    private void createInventory(Game game) {
        invMap = new LinkedHashMap<>();

        for (ALObject alObjectType : ALObject.values()) {
            addSlot(alObjectType, game, 0);
        }
    }

    /**
     * Called when updating inventory
     * updates each slot with a new game reference
     *
     * @param game A reference to the game for the update of all object slots
     */
    void updateInventory(Game game) {

        for (ALObject alObjectType : ALObject.values()) {
            updateSlot(alObjectType, game);
        }
    }

    /**
     * Called when the player attempts to select a slot
     * Assigns a slot to the local selectedSLot variable
     *
     * @param toSelect The slot the player has selected
     */
    public void selectSlot(InventorySlot toSelect) {
        if (toSelect == null) {
            this.selectedSlot = null;
        }

        for (Map.Entry<ALObject, InventorySlot> slot : invMap.entrySet()) {
            if (slot.getValue() == toSelect) {
                slot.getValue().setSelected(true);
                this.selectedSlot = slot.getValue();
                selectedChanged = true;
            }
            else {
                slot.getValue().setSelected(false);
            }
        }
    }

    /**
     * Has a slot been selected
     *
     * @return True if a slot has been selected, false otherwise
     */
    boolean isSelectedChanged() {
        if (selectedChanged) {
            selectedChanged = false;
            return true;
        }
        else
            return false;
    }

    /**
     * Get a slot in the inventory that is for a specific object type
     *
     * @param type The ALObject that the slot should contain
     * @return The inventory slot that contains the object
     */
    InventorySlot getSlot(ALObject type) {
        for (Map.Entry<ALObject, InventorySlot> slot : invMap.entrySet()) {
            if (slot.getKey() == type) {
                return slot.getValue();
            }
        }
        return null;
    }

    public LinkedHashMap<ALObject, InventorySlot> getInvMap() {
        return invMap;
    }

    public InventorySlot getSelectedSlot() {
        return selectedSlot;
    }

    void setSelectedChanged(boolean selectedChanged) {
        this.selectedChanged = selectedChanged;
    }
}

