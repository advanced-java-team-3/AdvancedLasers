package ui;

/**
 * Created by shaun on 24/10/2016.
 */
public interface ErrorListener {
    void onError(String message, Exception e);
}
