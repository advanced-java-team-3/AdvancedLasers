package ui;

import core.Game;
import core.Input;
import core.Inventory;
import core.InventorySlot;
import data.ALObject;
import data.InputCommand;
import data.Options;
import graphics.InventorySlotCanvas;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Map;
import java.util.Optional;

/**
 * Created by Timothy on 26/09/2016.
 */
public class GameUI extends Scene implements UIListener, ErrorListener  {
    private Stage stage;

    private BorderPane bp = new BorderPane();

    private TilePane centre = new TilePane();
    private TilePane left = new TilePane();
    private TilePane right = new TilePane();

    private Game game;
    private Input input;

    // Menu buttons
    private Label title = new Label("Advanced Lasers");
    private Separator separator1 = new Separator();
    private Button mainGameButton = new Button("Main game");
    private Button editorButton = new Button("Editor");
    private Button exitButton = new Button("Quit");
    private Separator separator2 = new Separator();
    private Label editorModeTitle = new Label("Editor mode");
    private Button saveButton = new Button("Save Current");
    private Label selectedItem = new Label("");
    private Label amountOfSelectedItem = new Label(": ");
    private Button increaseInventory = new Button("+");
    private Button decreaseInventory = new Button("-");
    private Button clearLevelButton = new Button("Clear");
    private Button debugButton = new Button("Debug");
    private Separator separator3 = new Separator();
    private Label levelFiles = new Label("<levelfiles>");

    // Level menu
    private Label levelTitle = new Label("<levelname>");
    private Button prevLevelButton = new Button("Previous");
    private Button nextLevelButton = new Button("Next");
    private GridPane tpInventory = new GridPane();
    private Text levelInformation = new Text("Targets to Hit: "+0);


    public GameUI(Group root, Stage stage, double width, double height, Canvas canvas) {
        super(root);

        this.stage = stage;
        this.stage.setWidth(width);
        this.stage.setHeight(height);
        this.stage.setResizable(false);

        resize();

        root.getChildren().add(bp);
        centre.getChildren().add(canvas);
        bp.setLeft(left);
        bp.setCenter(centre);
        bp.setRight(right);

        // ----------------------------------------------------------
        // Setup menu (left side)
        // ----------------------------------------------------------
        VBox vbMenu = new VBox();
        vbMenu.setSpacing(10);
        vbMenu.setPadding(new Insets(0, 20, 10, 20));
        vbMenu.setPrefWidth(200);
        vbMenu.getChildren().addAll(title, separator1, mainGameButton, editorButton, exitButton, debugButton, separator2,
                editorModeTitle, saveButton, clearLevelButton, separator3, levelFiles);

        // Set font
        title.setAlignment(Pos.BASELINE_CENTER);
        title.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        editorModeTitle.setAlignment(Pos.BASELINE_CENTER);
        editorModeTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 14));

        title.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        mainGameButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        editorButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        exitButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        editorModeTitle.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        saveButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        increaseInventory.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        decreaseInventory.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        debugButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        levelFiles.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        clearLevelButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        // Set buttons to MainGame
        mainGameButton.setDisable(true);
        separator2.setVisible(false);
        editorModeTitle.setVisible(false);
        increaseInventory.setVisible(false);
        decreaseInventory.setVisible(false);
        saveButton.setVisible(false);
        separator3.setVisible(false);
        levelFiles.setVisible(false);

        left.setAlignment(Pos.BASELINE_CENTER);
        left.getChildren().add(vbMenu);

        // ----------------------------------------------------------

        // ----------------------------------------------------------
        // Setup level menu (right side)
        // ----------------------------------------------------------
        VBox vbLevelMenu = new VBox();
        vbLevelMenu.setSpacing(10);
        vbLevelMenu.setPadding(new Insets(0, 20, 10, 20));
        vbLevelMenu.setMaxWidth(200);

        right.setAlignment(Pos.TOP_CENTER);
        right.getChildren().add(vbLevelMenu);

        levelTitle.setAlignment(Pos.BASELINE_CENTER);
        levelTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 12));

        VBox vbLevelTitleAndSelectMenu = new VBox();
        vbLevelTitleAndSelectMenu.setMaxWidth(Double.MAX_VALUE);

        HBox hbLevelButtons = new HBox();
        hbLevelButtons.setMaxWidth(Double.MAX_VALUE);

        vbLevelTitleAndSelectMenu.getChildren().add(levelTitle);
        vbLevelTitleAndSelectMenu.setAlignment(Pos.BASELINE_CENTER);
        vbLevelTitleAndSelectMenu.setMaxWidth(Double.MAX_VALUE);
        vbLevelMenu.getChildren().add(vbLevelTitleAndSelectMenu);
        vbLevelMenu.setMaxWidth(Double.MAX_VALUE);

        vbLevelTitleAndSelectMenu.getChildren().add(hbLevelButtons);
        hbLevelButtons.getChildren().addAll(prevLevelButton, nextLevelButton);
        hbLevelButtons.setAlignment(Pos.BASELINE_CENTER);
        hbLevelButtons.setPrefWidth(200);
        hbLevelButtons.setSpacing(10);
        //hbLevelButtons.setBorder(new Border(new BorderStroke(Color.BLACK,
        //      BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        prevLevelButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        prevLevelButton.setPrefWidth(90.0);
        nextLevelButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        nextLevelButton.setPrefWidth(90.0);

        Separator separator3 = new Separator();
        vbLevelMenu.getChildren().add(separator3);

        tpInventory.setHgap(5);
        tpInventory.setVgap(5);
        tpInventory.setPadding(new Insets(0, 10, 0, 10));

        vbLevelMenu.getChildren().add(tpInventory);

        HBox hbItemDesc = new HBox();
        hbItemDesc.setAlignment(Pos.BASELINE_RIGHT);
        hbItemDesc.setPrefWidth(200);
        hbItemDesc.setSpacing(10);

        //selectedSlot.getAlObjectType().toString();

        amountOfSelectedItem.setVisible(false);
        hbItemDesc.getChildren().addAll(selectedItem, amountOfSelectedItem, increaseInventory, decreaseInventory);
        vbLevelMenu.getChildren().add(hbItemDesc);

        Separator separator5 = new Separator();
        vbLevelMenu.getChildren().add(separator5);

        TextFlow textLevelInfo = new TextFlow(levelInformation);
        textLevelInfo.setPrefWidth(200);
        vbLevelMenu.getChildren().add(textLevelInfo);



        // ----------------------------------------------------------
        // Setup actions
        // ----------------------------------------------------------
        clearLevelButton.setOnAction(e -> input.requestCommand(InputCommand.CLEAR));
        exitButton.setOnAction(e -> stage.close());
        mainGameButton.setOnAction(e -> input.requestCommand(InputCommand.MAINGAME));
        editorButton.setOnAction(e -> input.requestCommand(InputCommand.EDITOR));
        debugButton.setOnAction(e -> input.requestCommand(InputCommand.DEBUG));
        saveButton.setOnAction(e -> {
            if(!game.getWorld().getCurrentLevelName().equals("")) {
                input.requestCommand(InputCommand.SAVE_CURRENT);
            }
        });
        nextLevelButton.setOnAction(e -> input.requestCommand(InputCommand.LOAD_NEXT_LEVEL));
        prevLevelButton.setOnAction(e -> input.requestCommand(InputCommand.LOAD_PREVIOUS_LEVEL));
        increaseInventory.setOnAction(e -> input.requestCommand(InputCommand.INCREASE_INVENTORY));
        decreaseInventory.setOnAction(e -> input.requestCommand(InputCommand.DECREASE_INVENTORY));

        levelTitle.setOnMouseClicked(event -> {
            if(game.getGameMode() == Game.GameMode.EDITOR) {
                String oldTitle = game.getWorld().getSaveOverLevelName();

                TextInputDialog dialog = new TextInputDialog(oldTitle);
                dialog.setTitle("Edit level title");
                dialog.setContentText("New title:");
                dialog.initStyle(StageStyle.UTILITY);

                Optional<String> result = dialog.showAndWait();
                result.ifPresent(name -> {
                    if(!name.equals("")) {
                        game.getWorld().setLevelName(name);
                    }
                    else {
                        game.getWorld().setLevelName("");
                    }
                    game.getInput().requestCommand(InputCommand.UPDATE_UI);
                });
            }
        });
    }

    /**
     * Updates the inventory, called when the UIListener updates.
     * If in main gameplay mode then only displays inventory slots with 1 or
     * more items available to be placed
     * If in edit mode then displays all inventory slots
     */
    private void updateTpInventory() {
        tpInventory.getChildren().clear();

        Inventory inv = game.getWorld().getCurrentLevel().getInventory();
        Map<ALObject, InventorySlot> invMap = inv.getInvMap();

        int column = 0;
        int maxColumn = 4;
        int row = 0;

        for(Map.Entry<ALObject, InventorySlot> slot : invMap.entrySet()) {
            //only show the inventory items if in editor mode of it has more than 1 item in main mode
            if(game.getGameMode() == Game.GameMode.EDITOR || (game.getGameMode() == Game.GameMode.MAINGAME && slot.getValue().getMaxAmount()>0)) {
                InventorySlotCanvas isc = new InventorySlotCanvas(slot.getValue(), inv, input);
                tpInventory.add(isc.getCanvas(), column, row);

                if (++column == maxColumn) {
                    column = 0;
                    ++row;
                }
            }
        }
    }

    /**
     * Used to resize the game window.
     * At the moment only used on startup.
     * May not work when resizing windows during runtime.
     */
    private void resize() {
        double width = stage.getWidth();
        double height = stage.getHeight();

        double sideWidth = (width - Options.GAME_WIDTH) * 0.5;
        double newWidth = ((height + Options.GAME_HEIGHT - Options.SCREEN_HEIGHT)/Options.SCREEN_HEIGHT)
                *Options.GAME_WIDTH;
        double newHeight = ((width + Options.GAME_WIDTH - Options.SCREEN_WIDTH)/Options.SCREEN_WIDTH)
                *Options.GAME_HEIGHT;

        bp.setPrefWidth(width);
        centre.setPrefWidth(newWidth);
        left.setPrefWidth(sideWidth);
        right.setPrefWidth(sideWidth);

        bp.setPrefHeight(height);
        centre.setPrefHeight(newHeight);
        left.setPrefHeight(height);
        right.setPrefHeight(height);
    }

    /**
     * Sets the game with a new game reference.
     * Called after the game engine has been created
     * @param game new game reference
     */
    public void setGame(Game game) {
        this.game = game;
        this.input = game.getInput();
        this.game.addMyChangeListener(this);
        this.game.addErrorListener(this);
    }

    /**
     * Updates all Game UI buttons and text followed by the levels inventory
     * @param evt The evnt that caused the Ui to update. Not used.
     */
    @Override
    public void UIListener(UpdateUIEvent evt) {
        if(game.getGameMode() == Game.GameMode.MAINGAME) {
            mainGameButton.setDisable(true);
            editorButton.setDisable(false);

            separator2.setVisible(false);
            editorModeTitle.setVisible(false);
            saveButton.setVisible(false);
            levelFiles.setVisible(false);
            clearLevelButton.setVisible(false);

            levelTitle.setStyle("-fx-border-color: black; -fx-border-style: hidden;");
        }
        else {
            mainGameButton.setDisable(false);
            editorButton.setDisable(true);
            separator2.setVisible(true);
            editorModeTitle.setVisible(true);
            saveButton.setVisible(true);

            String levelName = game.getWorld().getSaveOverLevelName();
            if(levelName.equals("")) {
                saveButton.setDisable(true);
                saveButton.setText("Save level: <no name>");
            }
            else {
                saveButton.setDisable(false);
                saveButton.setText("Save level: "+ levelName);
            }

            //savePrevious.setVisible(true);
            //saveNext.setVisible(true);
            //saveNewButton.setVisible(true);
            levelFiles.setVisible(true);
            clearLevelButton.setVisible(true);
            levelFiles.setText(game.getWorld().getEditorLevelNames());
            InventorySlot selectedSlot = game.getWorld().getCurrentLevel().getInventory().getSelectedSlot();
            if(selectedSlot == null) {
                selectedItem.setVisible(false);
                increaseInventory.setVisible(false);
                decreaseInventory.setVisible(false);
                amountOfSelectedItem.setVisible(false);
            }
            else {
                String s = selectedSlot.getAlObjectType().toString();
                if(s.length() > 14) {
                    s = s.substring(0, Math.min(s.length(), 14)) + "..";
                }

                selectedItem.setText(s);
                selectedItem.setVisible(true);
                amountOfSelectedItem.setText(": " + selectedSlot.getMaxAmount());
                amountOfSelectedItem.setVisible(true);
                increaseInventory.setVisible(true);
                decreaseInventory.setVisible(true);
            }

            levelTitle.setStyle("-fx-border-color: black; -fx-border-style: solid;");
        }
        String lvTitle = game.getWorld().getCurrentLevelName();


        if(lvTitle.equals("")) {
            lvTitle = "<no title>";
        }
        levelTitle.setText(lvTitle);
        levelInformation.setText(game.getWorld().getLevelInformation());
        // Update inventory
        updateTpInventory();
    }

    @Override
    public void onError(String message, Exception e) {
        System.err.println(message);

        if (e != null)
            e.printStackTrace();

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Advanced Lasers");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }
}
