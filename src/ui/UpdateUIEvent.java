package ui;

import java.util.EventObject;

/**
 * Created by Timothy on 30/09/2016.
 */

public class UpdateUIEvent extends EventObject {
    // This event definition is stateless but you could always
    // add other information here.
    public UpdateUIEvent(Object source) {
        super(source);
    }
}