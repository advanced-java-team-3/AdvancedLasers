package ui;

import core.World;
import data.Options;
import javafx.scene.canvas.GraphicsContext;

/**
 * Created by shaun on 24/09/2016.
 */
public class Grid {
    private World world;

    public Grid(World world) {
        this.world = world;
    }

    /**
     * Draws the grid to the levels canvas.
     */
    public void draw() {
        GraphicsContext ctx = world.getGame().getRenderer().getGraphics();

        ctx.save();

        double width = world.getGame().getScreen().getWidth();
        double height = world.getGame().getScreen().getHeight();

        double dx = world.getIncrements().getX();
        double dy = world.getIncrements().getY();

        ctx.setFill(Options.BACKGROUND_COLOR);
        ctx.fillRect(0, 0, width, height);

        ctx.setStroke(Options.GRID_LINE_COLOR);
        ctx.setLineWidth(Options.GRID_LINE_WIDTH);

        ctx.setLineDashes(
                Options.GRID_LINE_DASH, Options.GRID_LINE_DASH_SPACING);

        for (double i = 0; i <= width; i += dx) {
            ctx.strokeLine(i, 0, i, height);
        }

        for (double i = 0; i <= height; i += dy) {
            ctx.strokeLine(0, i, width, i);
        }

        ctx.restore();
    }
}
